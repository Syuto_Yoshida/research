% This script uses lstmLayer to train the model. 
% This model takes audio segment as input and outputs the roughness.
% 
% segmentNum : Number of segments obtained by splitting from a single 1000 ms audio
% audioLen : length of audio segment for trainig
% sr : sample rate
% 
% AUTHOR    : Syuto Yoshida
% $DATE     : 2023-9-22
% $Revision : 1.00 $
% DEVELOPED : R2022b

segmentNum = 9;
audioLen = 200;
sr = 48000; 

[XTrain, YTrain, XTest, YTest, XValidation, YValidation] = get_training_data(segmentNum, audioLen, sr);

inputSize = [1 sr*audioLen/1000 1];
% inputSize = 9600;
numHiddenUnits = 100;
layers = [
    sequenceInputLayer(inputSize,'Name','input')
    flattenLayer('Name','flatten')
    lstmLayer(numHiddenUnits,'OutputMode','last','Name','lstm')
    dropoutLayer(0.5)
    fullyConnectedLayer(1,'Name','fc')
    regressionLayer('Name','reg')
    ];

options = trainingOptions('adam', ...
    ExecutionEnvironment = "auto", ...
    InitialLearnRate = 0.02, ...
    LearnRateSchedule = "none", ...
    LearnRateDropPeriod=5, ...
    LearnRateDropFactor=0.5, ...
    MaxEpochs = 70, ...
    MiniBatchSize = 256, ...
    L2Regularization=0.0000001, ...
    GradientThreshold = Inf, ...
    Shuffle = 'every-epoch', ...
    Verbose = 1, ...
    ValidationData = {XValidation,YValidation}, ...  
    Plots = "training-progress");

net = trainNetwork(XTrain,YTrain,layers,options);