% This script creates a model that predicts roughness using CNN with
% recursion. The training data is an audio file of 1 seconds in length
% sampled at 48 kHz. Each audio file has a roughness value as teaching data.
%
% N: Toatal number of data
% XTrain[row, colum, chanel, number of data]
% YTrain[number of data, roughness value(asper)]
% rmse: Root Mean Squared Error
%
% REMARKS
%
% AUTHOR    : Syuto Yoshida
% $DATE     : 2023-3-19
% $Revision : 1.00 $
% DEVELOPED : Version of Matlab

files1 = dir('audio/audio_SAM_modulation_frequency/*.wav');
files2 = dir('audio/audio_SAM_modulation_depth/*.wav');
files3 = dir('audio/audio_unmodulated_bandpass_noise/*.wav');
files4 = dir('audio/audio_FM_modulation_frequency/*.wav');
files5 = dir('audio/audio_FM_frequency_deviation/*.wav');
files6 = dir('audio/audio_FM_dB/*.wav');
files7 = dir('audio/audio_musical_scale/*.wav');
files8 = dir('audio/audio_SAM_dB/*.wav');
files9 = dir('audio/audio_pSAM_phase/*.wav');
totalFiles = height(struct2table(files1)) + height(struct2table(files2))...
    + height(struct2table(files3)) + height(struct2table(files4))... 
    + height(struct2table(files5)) + height(struct2table(files6))...
    + height(struct2table(files7)) + height(struct2table(files8)) + height(struct2table(files9));

totalData = totalFiles*9;

sr = 48000;       % sample rate
audioLen = 200;  % audio length (ms)
sampleNum = sr * audioLen / 1000;
XTrain = zeros(1, sampleNum, 1, totalData); 
YTrain = zeros(totalData,1);

n=1;

T = readtable('roughness_data/roughness_training_data_SAM_modulation_frequency.xlsx');
numfiles = height(struct2table(files1));
for i=1:numfiles
    filename = append('audio/audio_SAM_modulation_frequency/audio', int2str(i), '.wav');
    [x, Fs] = audioread(filename);
    startSample=1;
    endSample=sampleNum;
    for j=1:9
        XTrain(:,:,:,n) = x(startSample:endSample).';
        YTrain(n) = table2array(T(i,2));
        startSample=startSample+sampleNum/2;
        endSample=endSample+sampleNum/2;
        n=n+1;
    end
end

T = readtable('roughness_data/roughness_training_data_SAM_modulation_depth.xlsx');
numfiles = height(struct2table(files2));
for i=1:numfiles
    [x, Fs] = audioread(append('audio/audio_SAM_modulation_depth/audio', int2str(i), '.wav'));
    startSample=1;
    endSample=sampleNum;
    for j=1:9
        XTrain(:,:,:,n) = x(startSample:endSample).';
        YTrain(n) = table2array(T(i,2));
        startSample=startSample+sampleNum/2;
        endSample=endSample+sampleNum/2;
        n=n+1;
    end
end

T = readtable('roughness_data/roughness_training_data_unmodulated_bandpass_noise.xlsx');
numfiles = height(struct2table(files3));
for i=1:numfiles
    [x, Fs] = audioread(append('audio/audio_unmodulated_bandpass_noise/audio', int2str(i), '.wav'));
    startSample=1;
    endSample=sampleNum;
    for j=1:9
        XTrain(:,:,:,n) = x(startSample:endSample).';
        YTrain(n) = table2array(T(i,2));
        startSample=startSample+sampleNum/2;
        endSample=endSample+sampleNum/2;
        n=n+1;
    end
end

T = readtable('roughness_data/roughness_training_data_FM_modulation_frequency.xlsx');
numfiles = height(struct2table(files4));
for i=1:numfiles
    [x, Fs] = audioread(append('audio/audio_FM_modulation_frequency/audio', int2str(i), '.wav'));
    startSample=1;
    endSample=sampleNum;
    for j=1:9
        XTrain(:,:,:,n) = x(startSample:endSample).';
        YTrain(n) = table2array(T(i,2));
        startSample=startSample+sampleNum/2;
        endSample=endSample+sampleNum/2;
        n=n+1;
    end
end

T = readtable('roughness_data/roughness_training_data_FM_frequency_deviation.xlsx');
numfiles = height(struct2table(files5));
for i=1:numfiles
    [x, Fs] = audioread(append('audio/audio_FM_frequency_deviation/audio', int2str(i), '.wav'));
    startSample=1;
    endSample=sampleNum;
    for j=1:9
        XTrain(:,:,:,n) = x(startSample:endSample).';
        YTrain(n) = table2array(T(i,2));
        startSample=startSample+sampleNum/2;
        endSample=endSample+sampleNum/2;
        n=n+1;
    end
end

T = readtable('roughness_data/roughness_training_data_FM_dB.xlsx');
numfiles = height(struct2table(files6));
for i=1:numfiles
    [x, Fs] = audioread(append('audio/audio_FM_dB/audio', int2str(i), '.wav'));
    startSample=1;
    endSample=sampleNum;
    for j=1:9
        XTrain(:,:,:,n) = x(startSample:endSample).';
        YTrain(n) = table2array(T(i,2));
        startSample=startSample+sampleNum/2;
        endSample=endSample+sampleNum/2;
        n=n+1;
    end
end

% T = readtable('roughness_data/roughness_training_data_SFM_modulation_frequency.xlsx');
% numfiles = height(struct2table(files7));
% for i=1:numfiles
%     [x, Fs] = audioread(append('audio_SFM_modulation_frequency/audio', int2str(i), '.wav'));
%     startSample=1;
%     endSample=sampleNum;
%     N = n;
%     for j=N:N+8
%         XTrain(:,:,:,j) = x(startSample:endSample).';
%         YTrain(n) = table2array(T(i,2));
%         startSample=startSample+sampleNum/2;
%         endSample=endSample+sampleNum/2;
%         n=n+1;
%     end
% end
% 
% T = readtable('roughness_data/roughness_training_data_SFM_frequency_division.xlsx');
% numfiles = height(struct2table(files8));
% for i=1:numfiles
%     [x, Fs] = audioread(append('audio_SFM_frequency_division/audio', int2str(i), '.wav'));
%     startSample=1;
%     endSample=sampleNum;
%     N = n;
%     for j=N:N+8
%         XTrain(:,:,:,j) = x(startSample:endSample).';
%         YTrain(n) = table2array(T(i,2));
%         startSample=startSample+sampleNum/2;
%         endSample=endSample+sampleNum/2;
%         n=n+1;
%     end
% end

T = readtable('roughness_data/roughness_training_data_musical_scale.xlsx');
numfiles = height(struct2table(files7));
for i=1:numfiles
    [x, Fs] = audioread(append('audio/audio_musical_scale/audio', int2str(i), '.wav'));
    startSample=1;
    endSample=sampleNum;
    for j=1:9
        XTrain(:,:,:,n) = x(startSample:endSample).';
        YTrain(n) = table2array(T(i,2));
        startSample=startSample+sampleNum/2;
        endSample=endSample+sampleNum/2;
        n=n+1;
    end
end

T = readtable('roughness_data/roughness_training_data_SAM_dB.xlsx');
numfiles = height(struct2table(files8));
for i=1:numfiles
    [x, Fs] = audioread(append('audio/audio_SAM_dB/audio', int2str(i), '.wav'));
    startSample=1;
    endSample=sampleNum;
    for j=1:9
        XTrain(:,:,:,n) = x(startSample:endSample).';
        YTrain(n) = table2array(T(i,2));
        startSample=startSample+sampleNum/2;
        endSample=endSample+sampleNum/2;
        n=n+1;
    end
end

T = readtable('roughness_data/roughness_training_data_pSAM_phase.xlsx');
numfiles = height(struct2table(files9));
for i=1:numfiles
    [x, Fs] = audioread(append('audio/audio_pSAM_phase/audio', int2str(i), '.wav'));
    startSample=1;
    endSample=sampleNum;
    for j=1:9
        XTrain(:,:,:,n) = x(startSample:endSample).';
        YTrain(n) = table2array(T(i,2));
        startSample=startSample+sampleNum/2;
        endSample=endSample+sampleNum/2;
        n=n+1;
    end
end

% 10% data each for validation and testing
validIdx = randperm(totalData,round(totalData*0.1));
XValidation = XTrain(:,:,:,validIdx);
XTrain(:,:,:,validIdx) = [];
testIdx = randperm(totalData-round(totalData*0.1),round(totalData*0.1));
XTest = XTrain(:,:,:,testIdx);
XTrain(:,:,:,testIdx) = [];
YValidation = YTrain(validIdx);
YTrain(validIdx) = [];
YTest = YTrain(testIdx);
YTrain(testIdx) = [];

% Layer design
layer = [
    imageInputLayer([1 sampleNum 1])
    convolution2dLayer([1 512],10,'Stride',[1 10],'Padding','same')
    batchNormalizationLayer
    reluLayer
    maxPooling2dLayer([1 2],'Stride',2)
    convolution2dLayer([1 256],20,'Stride',[1 5],'Padding','same')
    batchNormalizationLayer
    reluLayer
    maxPooling2dLayer([1 2],'Stride',2)
    convolution2dLayer([1 128],40,'Stride',[1 2],'Padding','same')
    batchNormalizationLayer
    reluLayer
    maxPooling2dLayer([1 2],'Stride',2)
    convolution2dLayer([1 64],60,'Stride',[1 2],'Padding','same')
    batchNormalizationLayer
    reluLayer
    maxPooling2dLayer([1 2],'Stride',2)
    convolution2dLayer([1 32],80,'Stride',1,'Padding','same')
    batchNormalizationLayer
    reluLayer
    maxPooling2dLayer([1 2],'Stride',2)
    dropoutLayer(0.5)
    fullyConnectedLayer(1)
    regressionLayer];

options = trainingOptions("rmsprop", ...
    InitialLearnRate=0.001, ...
    LearnRateSchedule="piecewise", ...
    MaxEpochs=25, ...
    MiniBatchSize=128, ...
    L2Regularization=1.0000e-04, ...
    ValidationData={XValidation,YValidation}, ...
    ValidationFrequency=30, ...
    Shuffle='every-epoch', ...
    Plots="training-progress");

net = trainNetwork(XTrain,YTrain,layer,options);

YPred = predict(net,XTest);
rmse = sqrt(mean((YTest-YPred).^2)) % Root Mean Squared Error
%% Test the roughness of dependence of modulation rate in SAM tone 
% Daniel and Weber Fig.3 
% fc=125(Hz), m=1, 60dB

N = 101; 
YPred_DanielAndWeber_Fig3_125 = zeros(1, N);
acoustic_roughness_DanielAndWeber_Fig3_125 = zeros(1, N);


for n=1:N
    fileName = append('audio/audio_SAM_modulation_frequency/audio', int2str(n), '.wav');
    [x, Fs] = audioread(fileName);
    YPred_DanielAndWeber_Fig3_125(1, n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    roughness = median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]);
    acoustic_roughness_DanielAndWeber_Fig3_125(1, n) = mean(roughness);
end

XFmod_DanielAndWeber_Fig3_125 = 0:N-1; % x-axis value

% Daniel and Weber Fig.3 
% fc=500(Hz), m=1, 60dB

N = 201;
YPred_DanielAndWeber_Fig3_500 = zeros(1, N);
acoustic_roughness_DanielAndWeber_Fig3_500 = zeros(1, N);

for n=1:N
    fileName = append('audio/audio_SAM_modulation_frequency/audio', int2str(n+101), '.wav');
    [x, Fs] = audioread(fileName);
    YPred_DanielAndWeber_Fig3_500(1, n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    roughness = median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]);
    acoustic_roughness_DanielAndWeber_Fig3_500(1, n) = mean(roughness);
end

XFmod_DanielAndWeber_Fig3_500 = 0:N-1; % x-axis value

% Daniel and Weber Fig.3 
% fc=8000(Hz), m=1, 60dB

N = 301;
YPred_DanielAndWeber_Fig3_8000 = zeros(1, N);
acoustic_roughness_DanielAndWeber_Fig3_8000 = zeros(1, N);

for n=1:N
    fileName = append('audio/audio_SAM_modulation_frequency/audio', int2str(n+302), '.wav');
    [x, Fs] = audioread(fileName);
    YPred_DanielAndWeber_Fig3_8000(n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    roughness = median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]);
    acoustic_roughness_DanielAndWeber_Fig3_8000(n) = mean(roughness);
end

XFmod_DanielAndWeber_Fig3_8000 = 0:N-1; % x-axis value

% Daniel and Weber Fig.3 
% fc=2000(Hz), m=1, 60dB

N=301;
YPred_DanielAndWeber_Fig3_2000 = zeros(1, N);
acoustic_roughness_DanielAndWeber_Fig3_2000 = zeros(1, N);

for n=1:N
    fileName = append('audio/audio_SAM_modulation_frequency/audio', int2str(n+603), '.wav');
    [x, Fs] = audioread(fileName);
    YPred_DanielAndWeber_Fig3_2000(n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    roughness = median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]);
    acoustic_roughness_DanielAndWeber_Fig3_2000(n) = mean(roughness);
end

XFmod_DanielAndWeber_Fig3_2000 = 0:N-1; % x-axis value

% Daniel and Weber Fig.3 
% fc=1000(Hz), m=1, 60dB

N=301;
YPred_DanielAndWeber_Fig3_1000 = zeros(1, N);
acoustic_roughness_DanielAndWeber_Fig3_1000 = zeros(1, N);

for n=1:N
    fileName = append('audio/audio_SAM_modulation_frequency/audio', int2str(n+904), '.wav');
    [x, Fs] = audioread(fileName);
    YPred_DanielAndWeber_Fig3_1000(n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    roughness = median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]);
    acoustic_roughness_DanielAndWeber_Fig3_1000(n) = mean(roughness);
end

XFmod_DanielAndWeber_Fig3_1000 = 0:N-1; % x-axis value

% Daniel and Weber Fig.3 
% fc=4000(Hz), m=1, 60dB

N=301; 
YPred_DanielAndWeber_Fig3_4000 = zeros(1, N);
acoustic_roughness_DanielAndWeber_Fig3_4000 = zeros(1, N);

for n=1:N
    fileName = append('audio/audio_SAM_modulation_frequency/audio', int2str(n+1205), '.wav');
    [x, Fs] = audioread(fileName);
    YPred_DanielAndWeber_Fig3_4000(n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    roughness = median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]);
    acoustic_roughness_DanielAndWeber_Fig3_4000(n) = mean(roughness);
end

XFmod_DanielAndWeber_Fig3_4000 = 0:N-1; % x-axis value

% Daniel and Weber Fig.3 
% fc=250(Hz), m=1, 60dB

N=201;
YPred_DanielAndWeber_Fig3_250 = zeros(1, N);
acoustic_roughness_DanielAndWeber_Fig3_250 = zeros(1, N);

for n=1:N
    fileName = append('audio/audio_SAM_modulation_frequency/audio', int2str(n+1506), '.wav');
    [x, Fs] = audioread(fileName);
    YPred_DanielAndWeber_Fig3_250(n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    roughness = median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]);
    acoustic_roughness_DanielAndWeber_Fig3_250(n) = mean(roughness);
end

XFmod_DanielAndWeber_Fig3_250 = 0:N-1; % x-axis value

% Subjective Value

T = readtable('roughness_test_sub_data.xlsx');

%　Data preparation 
y_sub_DanielAndWeber_Fig3_125 = table2array(T(1:9,2));
x_sub_DanielAndWeber_Fig3_125 = linspace(20, 100, 9);

y_sub_DanielAndWeber_Fig3_500 = table2array(T(10:24,2));
x_sub_DanielAndWeber_Fig3_500 = linspace(20, 160, 15);

y_sub_DanielAndWeber_Fig3_8000 = table2array(T(25:39,2));
x_sub_DanielAndWeber_Fig3_8000 = linspace(20, 160, 15);

y_sub_DanielAndWeber_Fig3_2000 = table2array(T(40:54,2));
x_sub_DanielAndWeber_Fig3_2000 = linspace(20, 160, 15);

y_sub_DanielAndWeber_Fig3_1000 = table2array(T(55:69,2));
x_sub_DanielAndWeber_Fig3_1000 = linspace(20, 160, 15);

y_sub_DanielAndWeber_Fig3_4000 = table2array(T(70:84,2));
x_sub_DanielAndWeber_Fig3_4000 = linspace(20, 160, 15);

y_sub_DanielAndWeber_Fig3_250 = table2array(T(85:96,2));
x_sub_DanielAndWeber_Fig3_250 = linspace(20, 130, 12);

figure('Position',[0 0 600 850]);

t = tiledlayout(4,2,'TileSpacing','Compact','Padding','Compact');

t1 = nexttile;
plot(XFmod_DanielAndWeber_Fig3_125, YPred_DanielAndWeber_Fig3_125, 'k-', DisplayName='ML model',LineWidth=1.5)
hold on;
plot(XFmod_DanielAndWeber_Fig3_125, acoustic_roughness_DanielAndWeber_Fig3_125, 'b--+', DisplayName='standardized model',LineWidth=1)
plot(x_sub_DanielAndWeber_Fig3_125, y_sub_DanielAndWeber_Fig3_125, 'b--o', DisplayName='subjective',LineWidth=1);
title(t1,'$f_{c}$=$125$\,Hz','Interpreter','latex','FontSize',16);
window = legend('show','Location','northeast');
set(window, 'Interpreter','latex')
% xlabel(t, '$f_{mod}$\,[Hz]', 'Interpreter','latex','FontSize',16);
% ylabel(t, '$R$\,[asper]', 'Interpreter','latex','FontSize',16);
% xlim([0 300]);
ylim([0 0.6]);
set(gca,'FontSize',16,'FontName','Times New Roman');

t2 = nexttile;
plot(XFmod_DanielAndWeber_Fig3_250, YPred_DanielAndWeber_Fig3_250, 'k-', DisplayName='ML model', LineWidth=1.5)
hold on;
plot(XFmod_DanielAndWeber_Fig3_250, acoustic_roughness_DanielAndWeber_Fig3_250, 'b--+', DisplayName='standardized model',LineWidth=1)
plot(x_sub_DanielAndWeber_Fig3_250, y_sub_DanielAndWeber_Fig3_250, 'b--o', DisplayName='subjective',LineWidth=1);
title(t2,'$f_{c}$=$250$\,Hz','Interpreter','latex','FontSize',16);
% xlabel(t, '$f_{mod}$\,[Hz]', 'Interpreter','latex','FontSize',16);
% ylabel(t, '$R$\,[asper]', 'Interpreter','latex','FontSize',16);
% xlim([0 300]);
% ylim([0 1.2]);
set(gca,'FontSize',16,'FontName','Times New Roman');

t3 = nexttile;
plot(XFmod_DanielAndWeber_Fig3_500, YPred_DanielAndWeber_Fig3_500, 'k-', DisplayName='ML model', LineWidth=1.5)
hold on;
plot(XFmod_DanielAndWeber_Fig3_500, acoustic_roughness_DanielAndWeber_Fig3_500, 'b--+', DisplayName='standardized model', LineWidth=1)
plot(x_sub_DanielAndWeber_Fig3_500, y_sub_DanielAndWeber_Fig3_500, 'b--o', DisplayName='subjective',LineWidth=1);
title(t3,'$f_{c}$=$500$\,Hz','Interpreter','latex','FontSize',16);
% xlabel(t, '$f_{mod}$\,[Hz]', 'Interpreter','latex','FontSize',16);
% ylabel(t, '$R$\,[asper]', 'Interpreter','latex','FontSize',16);
% xlim([0 300]);
ylim([0 1]);
set(gca,'FontSize',16,'FontName','Times New Roman');

t4 = nexttile;
plot(XFmod_DanielAndWeber_Fig3_1000, YPred_DanielAndWeber_Fig3_1000, 'k-', DisplayName='ML model', LineWidth=1.5)
hold on;
plot(XFmod_DanielAndWeber_Fig3_1000, acoustic_roughness_DanielAndWeber_Fig3_1000, 'b--+', DisplayName='standardized model', LineWidth=1)
plot(x_sub_DanielAndWeber_Fig3_1000, y_sub_DanielAndWeber_Fig3_1000, 'b--o', DisplayName='subjective',LineWidth=1);
title(t4,'$f_{c}$=$1000$\,Hz','Interpreter','latex','FontSize',16);
% xlabel(t, '$f_{mod}$\,[Hz]', 'Interpreter','latex','FontSize',16);
% ylabel(t, '$R$\,[asper]', 'Interpreter','latex','FontSize',16);
% xlim([0 300]);
% ylim([0 1.2]);
set(gca,'FontSize',16,'FontName','Times New Roman');

t5 = nexttile;
plot(XFmod_DanielAndWeber_Fig3_2000, YPred_DanielAndWeber_Fig3_2000, 'k-', DisplayName='ML model', LineWidth=1.5)
hold on;
plot(XFmod_DanielAndWeber_Fig3_2000, acoustic_roughness_DanielAndWeber_Fig3_2000, 'b--+', DisplayName='ISO 532-1', LineWidth=1)
plot(x_sub_DanielAndWeber_Fig3_2000, y_sub_DanielAndWeber_Fig3_2000, 'b--o', DisplayName='subjective',LineWidth=1);
title(t5,'$f_{c}$=$2000$\,Hz','Interpreter','latex','FontSize',16);
% xlabel(t, '$f_{mod}$\,[Hz]', 'Interpreter','latex','FontSize',16);
% ylabel(t, '$R$\,[asper]', 'Interpreter','latex','FontSize',16);
% xlim([0 300]);
% ylim([0 1.2]);
set(gca,'FontSize',16,'FontName','Times New Roman');

t6 = nexttile;
plot(XFmod_DanielAndWeber_Fig3_4000, YPred_DanielAndWeber_Fig3_4000, 'k-', DisplayName='ML model', LineWidth=1.5)
hold on;
plot(XFmod_DanielAndWeber_Fig3_4000, acoustic_roughness_DanielAndWeber_Fig3_4000, 'b--+', DisplayName='ISO 532-1',LineWidth=1)
plot(x_sub_DanielAndWeber_Fig3_4000, y_sub_DanielAndWeber_Fig3_4000, 'b--o', DisplayName='subjective',LineWidth=1);
title(t6,'$f_{c}$=$4000$\,Hz','Interpreter','latex','FontSize',16);
% xlabel(t, '$f_{mod}$\,[Hz]', 'Interpreter','latex','FontSize',16);
% ylabel(t, '$R$\,[asper]', 'Interpreter','latex','FontSize',16);
% xlim([0 300]);
ylim([0 1]);
set(gca,'FontSize',16,'FontName','Times New Roman');

t7 = nexttile;
plot(XFmod_DanielAndWeber_Fig3_8000, YPred_DanielAndWeber_Fig3_8000, 'k-', DisplayName='ML model', LineWidth=1.5)
hold on;
plot(XFmod_DanielAndWeber_Fig3_8000, acoustic_roughness_DanielAndWeber_Fig3_8000, 'b--+', DisplayName='ISO 532-1', LineWidth=1)
plot(x_sub_DanielAndWeber_Fig3_8000, y_sub_DanielAndWeber_Fig3_8000, 'b--o', DisplayName='subjective',LineWidth=1);
title(t7,'$f_{c}$=$8000$\,Hz','Interpreter','latex','FontSize',16);
% xlabel(t, '$f_{mod}$\,[Hz]', 'Interpreter','latex','FontSize',16);
% ylabel(t, '$R$\,[asper]', 'Interpreter','latex','FontSize',16);
% xlim([0 300]);
ylim([0 0.5]);
set(gca,'FontSize',16,'FontName','Times New Roman');

xlabel(t, '$f_{mod}$\,[Hz]', 'Interpreter','latex','FontSize',16);
ylabel(t, '$R$\,[asper]', 'Interpreter','latex','FontSize',16);
%% Test roughness in dependence on modulation frequency in SAM tone
% Daniel and Weber Fig.4 

N = 684;

YPred_DanielAndWeber_Fig4 = zeros(1, N);
acoustic_roughness_DanielAndWeber_Fig4 = zeros(1, N);

for n=1:N
    fileName = append('audio_SAM_modulation_frequency/audio', int2str(1707+n), '.wav');
    [x, Fs] = audioread(fileName);
    YPred_DanielAndWeber_Fig4(n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    roughness = median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]);
    acoustic_roughness_DanielAndWeber_Fig4(n) = mean(roughness);
end

x = linspace(15,32,171);
T = readtable('roughness_test_sub_data.xlsx');
y_sub_DanielAndWeber_Fig4 = table2array(T(97:108,2));

figure('Name','Daniel and Weber Fig.4','NumberTitle','off');

subplot(1,4,1);
plot(x, YPred_DanielAndWeber_Fig4(1:171), 'k-', DisplayName='ML model', LineWidth=1);
hold on;
plot(x, acoustic_roughness_DanielAndWeber_Fig4(1:171), 'b--+', DisplayName='ISO 532-1', LineWidth=0.3);
plot([15 22 32], y_sub_DanielAndWeber_Fig4(1:3), 'b--o', DisplayName='subjective');
xticks([15 22 32]);
xticklabels({'15', '22', '32'});
title('250Hz');
xlabel('fm');

subplot(1,4,2);
plot(x, YPred_DanielAndWeber_Fig4(172:342), 'k-', DisplayName='ML model', LineWidth=1)
hold on;
plot(x, acoustic_roughness_DanielAndWeber_Fig4(172:342), 'b--+', DisplayName='ISO 532-1')
plot([15 22 32], y_sub_DanielAndWeber_Fig4(4:6), 'b--o', DisplayName='subjective');
xticks([15 22 32]);
xticklabels({'15', '22', '32'});
window = legend('show','Location','northeast');
set(window, 'Interpreter','latex')
title('500Hz');
xlabel('fm');

subplot(1,4,3);
plot(x, YPred_DanielAndWeber_Fig4(343:513), 'k-', DisplayName='ML model', LineWidth=1)
hold on;
plot(x, acoustic_roughness_DanielAndWeber_Fig4(343:513), 'b--+', DisplayName='ISO 532-1')
plot([15 22 32], y_sub_DanielAndWeber_Fig4(7:9), 'b--o', DisplayName='subjective');
xticks([15 22 32]);
xticklabels({'15', '22', '32'});
title('1kHz');
xlabel('fm');

subplot(1,4,4);
plot(x, YPred_DanielAndWeber_Fig4(514:684), 'k-', DisplayName='ML model', LineWidth=1)
hold on;
plot(x, acoustic_roughness_DanielAndWeber_Fig4(514:684), 'b--+', DisplayName='ISO 532-1')
plot([15 22 32], y_sub_DanielAndWeber_Fig4(10:12), 'b--o', DisplayName='subjective');
xticks([15 22 32]);
xticklabels({'15', '22', '32'});
title('5kHz');
xlabel('fm');
%% Test roughness in dependence on modulation depth in SAM tone
% Daniel and Weber Fig.5 
% fc=1000, fmod=70, 60dB

N = 101;

% XTestMDepth = zeros(1, sampleNum, 1, N); 
YPred_DanielAndWeber_Fig5 = zeros(1, N);
acoustic_roughness_DanielAndWeber_Fig5 = zeros(1, N);

for n=1:N
    fileName = append('audio/audio_SAM_modulation_depth/audio', int2str(n), '.wav');
    [x, Fs] = audioread(fileName);
    YPred_DanielAndWeber_Fig5(1, n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    roughness = median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]);
    acoustic_roughness_DanielAndWeber_Fig5(n) = mean(roughness);
end

XMDepth = linspace(0.1,1,N);

% subjective data
T = readtable('roughness_test_sub_data.xlsx');
y_sub_DanielAndWeber_Fig5 = table2array(T(109:118,2));
x_sub_DanielAndWeber_Fig5 = linspace(0.1, 1, 10);

set(groot, 'defaultAxesTickLabelInterpreter' , 'latex' ); 

figure('Position',[0 0 580 450]);

plot(XMDepth,YPred_DanielAndWeber_Fig5,'k-',DisplayName='ML model',LineWidth=1.5)
hold on;
plot(XMDepth, acoustic_roughness_DanielAndWeber_Fig5,'b--+',DisplayName='standardized model',LineWidth=1)
plot(x_sub_DanielAndWeber_Fig5,y_sub_DanielAndWeber_Fig5,'b--o',DisplayName='subjective',LineWidth=1);
window = legend('show','Location','northwest');
set(window, 'Interpreter','latex')
xlabel( '$m$', 'Interpreter','latex','FontSize',16);
ylabel('$R$\,[asper]', 'Interpreter','latex','FontSize',16);
set(gca,'FontSize',16,'FontName','Times New Roman');
% exportgraphics(gca, '../../roughness_research/master_thesis/result_cnn/result_cnn2/m_AM.pdf' );
exportgraphics(gca, '../../roughness_research/master_thesis/result_rnn/m_AM.pdf' );
%% Test roughness in dependence on band width in unmodulated bandpass noise
% Daniel and Weber Fig.8

N = 1575;

YPred_DanielAndWeber_Fig8 = zeros(1, N);
acoustic_roughness_DanielAndWeber_Fig8 = zeros(1, N);

XDanielAndWeber_Fig8_125 = linspace(10,250,25); % 1:25
XDanielAndWeber_Fig8_250 = linspace(10,500,50); % 26:75
XDanielAndWeber_Fig8_500 = linspace(10,1000,100); % 76:175
XDanielAndWeber_Fig8_1000 = linspace(10,2000,200); % 176:275, 276:375
XDanielAndWeber_Fig8_2000 = linspace(10,4000,400); % 376:475, 476:775
XDanielAndWeber_Fig8_4000 = linspace(10,8000,800); % 776:875, 876:1576

% i = 0;

for n=1:N
%     i=i+1;
%     if (276 <= n && 374 >= n)
%         i=i+1;
%         continue
%     end
%     if (476 <= n && 774 >= n)
%         i=i+1;
%         continue
%     end
%     if (876 <= n)
%         i=i+1;
%         continue
%     end
    fileName = append('audio/audio_unmodulated_bandpass_noise/audio', int2str(n), '.wav');
    [x, Fs] = audioread(fileName);
    YPred_DanielAndWeber_Fig8(n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    roughness = median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]);
    acoustic_roughness_DanielAndWeber_Fig8(n) = mean(roughness);
end

% subjective data
T = readtable('roughness_test_sub_data.xlsx');
y_sub_DanielAndWeber_Fig8 = table2array(T(119:159,2));
% error bar
err_neg = table2array(T(119:159,12));
err_pos = table2array(T(119:159,11));

set(groot, 'defaultAxesTickLabelInterpreter' , 'latex' ); 

figure('Position',[0 0 600 825]);

t = tiledlayout(3,2,'TileSpacing','Compact','Padding','Compact');

t1 = nexttile;
semilogx(XDanielAndWeber_Fig8_125, YPred_DanielAndWeber_Fig8(1:25), 'k-', DisplayName='ML model', LineWidth=1)
hold on;
semilogx(XDanielAndWeber_Fig8_125, acoustic_roughness_DanielAndWeber_Fig8(1:25), 'b--+', DisplayName='standardized model', LineWidth=0.5)
errorbar([10 30 100 200 250], y_sub_DanielAndWeber_Fig8(1:5), err_neg(1:5), err_pos(1:5), 'b--o', DisplayName='subjective', LineWidth=1);
xlim("padded");
xticks([10 100 1000])
xticklabels({'10', '100', '1000'});
ylim([0 0.5]);
title(t1,'$f_{c}$=$125$\,Hz', 'Interpreter','latex','FontSize',16);
set(gca,'FontSize',16,'FontName',  'Times New Roman');

t2 = nexttile;
semilogx(XDanielAndWeber_Fig8_250, YPred_DanielAndWeber_Fig8(26:75), 'k-', DisplayName='ML model', LineWidth=1)
hold on;
semilogx(XDanielAndWeber_Fig8_250, acoustic_roughness_DanielAndWeber_Fig8(26:75), 'b--+', DisplayName='standardized model', LineWidth=0.5)
errorbar([10 30 70 100 300 500], y_sub_DanielAndWeber_Fig8(6:11), err_neg(6:11), err_pos(6:11), 'b--o', DisplayName='subjective', LineWidth=1);
xlim("padded");
xticks([10 100 1000])
xticklabels({'10', '100', '1000'});
ylim([0 0.44]);
title(t2,'$f_{c}$=$250$\,Hz', 'Interpreter','latex','FontSize',16);
set(gca,'FontSize',16,'FontName',  'Times New Roman');

t3 = nexttile;
semilogx(XDanielAndWeber_Fig8_500, YPred_DanielAndWeber_Fig8(76:175), 'k-', DisplayName='ML model', LineWidth=1)
hold on;
semilogx(XDanielAndWeber_Fig8_500, acoustic_roughness_DanielAndWeber_Fig8(76:175), 'b--+', DisplayName='standardized model', LineWidth=0.5)
errorbar([10 30 70 100 300 1000], y_sub_DanielAndWeber_Fig8(12:17), err_neg(12:17), err_pos(12:17), 'b--o', DisplayName='subjective', LineWidth=1);
window = legend('show','Location','northwest');
set(window, 'Interpreter','latex')
xlim("padded");
xticks([10 100 1000])
xticklabels({'10', '100', '1000'});
ylim([0 0.44]);
title(t3,'$f_{c}$=$500$\,Hz', 'Interpreter','latex','FontSize',16);
set(gca,'FontSize',16,'FontName',  'Times New Roman');

t4 = nexttile;
semilogx(XDanielAndWeber_Fig8_1000, YPred_DanielAndWeber_Fig8(176:375), 'k-', DisplayName='ML model', LineWidth=1)
hold on;
semilogx(XDanielAndWeber_Fig8_1000, acoustic_roughness_DanielAndWeber_Fig8(176:375), 'b--+', DisplayName='standardized model', LineWidth=0.5)
errorbar([10 30 70 100 300 1000 2000], y_sub_DanielAndWeber_Fig8(18:24), err_neg(18:24), err_pos(18:24), 'b--o', DisplayName='subjective', LineWidth=1);
% legend('show');
xlim("padded");
xticks([10 100 1000])
xticklabels({'10', '100', '1000'});
ylim([0 0.44]);
title(t4,'$f_{c}$=$1$\,kHz', 'Interpreter','latex','FontSize',16);
set(gca,'FontSize',16,'FontName',  'Times New Roman');

t5 = nexttile;
semilogx(XDanielAndWeber_Fig8_2000, YPred_DanielAndWeber_Fig8(376:775), 'k-', DisplayName='ML model', LineWidth=1)
hold on;
semilogx(XDanielAndWeber_Fig8_2000, acoustic_roughness_DanielAndWeber_Fig8(376:775), 'b--+', DisplayName='standardized model', LineWidth=0.5)
errorbar([10 30 70 100 150 300 1000 2000 4000], y_sub_DanielAndWeber_Fig8(25:33), err_neg(25:33), err_pos(25:33), 'b--o', DisplayName='subjective', LineWidth=1);
% legend('show');
xlim("padded");
xticks([10 100 1000])
xticklabels({'10', '100', '1000'});
ylim([0 0.44]);
title(t5,'$f_{c}$=$2$\,kHz', 'Interpreter','latex','FontSize',16);
set(gca,'FontSize',16,'FontName',  'Times New Roman');

t6 = nexttile;
semilogx(XDanielAndWeber_Fig8_4000, YPred_DanielAndWeber_Fig8(776:1575), 'k-', DisplayName='ML model', LineWidth=1)
hold on;
semilogx(XDanielAndWeber_Fig8_4000, acoustic_roughness_DanielAndWeber_Fig8(776:1575), 'b--+', DisplayName='standardized model', LineWidth=0.5)
errorbar([10 30 70 100 300 1000 2000 8000], y_sub_DanielAndWeber_Fig8(34:41), err_neg(34:41), err_pos(34:41), 'b--o', DisplayName='subjective', LineWidth=1);
% legend('show');
xlim("padded");
xticks([10 100 1000])
xticklabels({'10', '100', '1000'});
ylim([0 0.44]);
title(t6,'$f_{c}$=$4$\,kHz', 'Interpreter','latex','FontSize',16);
set(gca,'FontSize',16,'FontName',  'Times New Roman');

xlabel(t, 'BW [Hz]','Interpreter','latex','FontSize',16);
ylabel(t, '$R$ [asper]','Interpreter','latex','FontSize',16);
%% Test roughness in dependence on modulation frequency in FM tone
% Daniel and Weber Fig.9 

N = 501;

YPred_DanielAndWeber_Fig9 = zeros(1, N);
acoustic_roughness_DanielAndWeber_Fig9 = zeros(1, N);

for n=1:N
    fileName = append('audio/audio_FM_modulation_frequency/audio', int2str(n), '.wav');
    [x, Fs] = audioread(fileName);
    YPred_DanielAndWeber_Fig9(n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    roughness = median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]);
    acoustic_roughness_DanielAndWeber_Fig9(n) = mean(roughness) * 100 / 3.207;
end

YPred_DanielAndWeber_Fig9 = YPred_DanielAndWeber_Fig9 * 100 / 3.207;
XDanielAndWeber_Fig9 = 0:500;

% subjective data
T = readtable('roughness_test_sub_data.xlsx');
y_sub_DanielAndWeber_Fig9 = table2array(T(160:171,2));
x_sub_DanielAndWeber_Fig9 = [4 10 20 40 60 80 100 120 150 200 300 500];
% error bar
err_neg = table2array(T(160:171,12));
err_pos = table2array(T(160:171,11));

set(groot, 'defaultAxesTickLabelInterpreter' , 'latex' ); 

figure('Position',[0 0 650 500]);

semilogx(XDanielAndWeber_Fig9, YPred_DanielAndWeber_Fig9, 'k-', DisplayName='ML model', LineWidth=1.5)
hold on;
semilogx(XDanielAndWeber_Fig9, acoustic_roughness_DanielAndWeber_Fig9, 'b--+', DisplayName='standardized model', LineWidth=1)
errorbar(x_sub_DanielAndWeber_Fig9, y_sub_DanielAndWeber_Fig9, err_neg, err_pos, 'b--o', DisplayName='subjective', LineWidth=1.5);
window = legend('show','Location','northwest');
set(window, 'Interpreter','latex')
xticks([10 100 1000])
xticklabels({'10', '100', '1000'});
xlim("padded");

xlabel('$f_{mod}$ [Hz]','Interpreter','latex','FontSize',16);
ylabel('$R/R_{0}$ [\%]','Interpreter','latex','FontSize',16);
set(gca,'FontSize',16,'FontName',  'Times New Roman');
% exportgraphics(gca, '../../roughness_research/master_thesis/result_cnn/mf_FM.pdf' );
exportgraphics(gca, '../../roughness_research/master_thesis/result_rnn/mf_FM.pdf' );
%% Test roughness in dependence on frequency division in FM tone
% Daniel and Weber Fig.10 

N = 773;

YPred_DanielAndWeber_Fig10 = zeros(1, n);
acoustic_roughness_DanielAndWeber_Fig10 = zeros(1, N);

for n=1:N
    fileName = append('audio/audio_FM_frequency_deviation/audio', int2str(n), '.wav');
    [x, Fs] = audioread(fileName);
    YPred_DanielAndWeber_Fig10(n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    roughness = median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]);
    acoustic_roughness_DanielAndWeber_Fig10(n) = mean(roughness) * 100 / 3.207;
end

YPred_DanielAndWeber_Fig10 = YPred_DanielAndWeber_Fig10 * 100 / 3.207;
XDanielAndWeber_Fig10 = [16 32 64 120 160 320 560 800 1000 1290 1440 1600];

% subjective data
T = readtable('roughness_test_sub_data.xlsx');
y_sub_DanielAndWeber_Fig10 = table2array(T(172:183,2));
x_sub_DanielAndWeber_Fig10 = [16 32 64 120 160 320 560 800 1000 1290 1440 1600];
% error bar
err_neg = table2array(T(172:183,12));
err_pos = table2array(T(172:183,11));

y_sub_DanielAndWeber_Fig10_2 = table2array(T(293:304,2));
x_sub_DanielAndWeber_Fig10_2 = [16 32 64 120 160 320 560 800 1000 1280 1440 1560];
% error bar
err_neg_2 = table2array(T(293:304,12));
err_pos_2 = table2array(T(293:304,11));

set(groot, 'defaultAxesTickLabelInterpreter' , 'latex' ); 

figure('Position',[0 0 600 300]);
t = tiledlayout(1,2,'TileSpacing','Compact','Padding','Compact');

t1 = nexttile;
semilogx(linspace(16,1560,773), YPred_DanielAndWeber_Fig10, 'k-', DisplayName='ML model', LineWidth=1.5)
hold on;
semilogx(linspace(16,1560,773), acoustic_roughness_DanielAndWeber_Fig10, 'b--+', DisplayName='standardized model', LineWidth=1.5)
errorbar(x_sub_DanielAndWeber_Fig10, y_sub_DanielAndWeber_Fig10, err_neg, err_pos, 'b--o', DisplayName='subjective', LineWidth=1);
title(t1,'(a)','Interpreter','latex','FontSize',16);
xlim("padded");
xticklabels({'100', '1000'});
ylim([0 300]);
set(gca,'FontSize',16,'FontName',  'Times New Roman');

t2 = nexttile;
semilogx(linspace(16,1560,773), YPred_DanielAndWeber_Fig10, 'k-', DisplayName='ML model', LineWidth=1.5)
hold on;
semilogx(linspace(16,1560,773), acoustic_roughness_DanielAndWeber_Fig10, 'b--+', DisplayName='standardized model', LineWidth=1.5)
errorbar(x_sub_DanielAndWeber_Fig10_2, y_sub_DanielAndWeber_Fig10_2, err_neg_2, err_pos_2, 'b--o', DisplayName='subjective', LineWidth=1);
title(t2,'(b)','Interpreter','latex','FontSize',16);
xlim("padded");
xticklabels({'100', '1000'});
ylim([0 300]);
window = legend('show','Location','northwest');
set(window, 'Interpreter','latex')

xlabel(t, '$\Delta f$\,[Hz]', 'Interpreter','latex','FontSize',16);
ylabel(t, '$R/R_{0}$\,[\%]', 'Interpreter','latex','FontSize',16);
set(gca,'FontSize',16,'FontName',  'Times New Roman');
%% Test roughness in dependence on Level(dB) in FM tone
% Daniel and Weber Fig.11 
% fc=1600, fmod=70, fdiv=800

N = 81;

YPred_DanielAndWeber_Fig11 = zeros(1, N);
acoustic_roughness_DanielAndWeber_Fig11 = zeros(1, N);

for n=1:N
    fileName = append('audio/audio_FM_dB/audio', int2str(n), '.wav');
    [x, Fs] = audioread(fileName);
    YPred_DanielAndWeber_Fig11(n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    roughness = median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]);
    acoustic_roughness_DanielAndWeber_Fig11(n) = mean(roughness) * 100 / 3.207;
end

YPred_DanielAndWeber_Fig11 = YPred_DanielAndWeber_Fig11 * 100 / 3.207;
XDanielAndWeber_Fig11 = linspace(40,80,N);

% subjective data
T = readtable('roughness_test_sub_data.xlsx');
y_sub_DanielAndWeber_Fig11 = table2array(T(184:188,2));
x_sub_DanielAndWeber_Fig11 = [40 50 60 70 80];
% error bar
err_neg = table2array(T(184:188,12));
err_pos = table2array(T(184:188,11));

figure('Position',[0 0 650 500]);

plot(XDanielAndWeber_Fig11, YPred_DanielAndWeber_Fig11, 'k-', DisplayName='ML model', LineWidth=1.5)
hold on;
plot(XDanielAndWeber_Fig11, acoustic_roughness_DanielAndWeber_Fig11, 'b--+', DisplayName='standardized model', LineWidth=1)
errorbar(x_sub_DanielAndWeber_Fig11, y_sub_DanielAndWeber_Fig11, err_neg, err_pos, 'b--o', DisplayName='subjective', LineWidth=1.5);
window = legend('show','Location','northwest');
set(window, 'Interpreter','latex')
xlabel('SPL\,[dB]', 'Interpreter','latex','FontSize',16);
ylabel('$R/R_{0}$\,[\%]', 'Interpreter','latex','FontSize',16);
xlim("padded");
set(gca,'FontSize',16,'FontName',  'Times New Roman');
exportgraphics(gca, '../../roughness_research/master_thesis/result_cnn/SPL_FM.pdf' );
% exportgraphics(gca, '../../roughness_research/master_thesis/result_rnn/SPL_FM.pdf' );
%% Test roughness in dependence on Level(dB) in SAM tone
% auditory roughness Fig.6.8

N = height(struct2table(files8));
 
YPred_auditory_roughness_Fig6_8 = zeros(1, N);
acoustic_roughness_auditory_roughness_Fig6_8 = zeros(1, N);

for n=1:N
    fileName = append('audio/audio_SAM_dB/audio', int2str(n), '.wav');
    [x, Fs] = audioread(fileName);
    YPred_auditory_roughness_Fig6_8(n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    roughness = median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]);
    acoustic_roughness_auditory_roughness_Fig6_8(n) = mean(roughness);
end

Xauditory_roughness_Fig6_8 = linspace(40, 80, 81);
YPred_auditory_roughness_Fig6_8 = YPred_auditory_roughness_Fig6_8 * 100 / 1.75438596491228;
acoustic_roughness_auditory_roughness_Fig6_8 = acoustic_roughness_auditory_roughness_Fig6_8 * 100 / 1.75438596491228;
% acoustic_roughness_auditory_roughness_Fig6_8 = acoustic_roughness_auditory_roughness_Fig6_8 * 100 / 2.26;

% subjective data
% T = readtable('roughness_test_sub_data.xlsx');


figure('Name','auditory roughness Fig.6.8','NumberTitle','off');

plot(Xauditory_roughness_Fig6_8, YPred_auditory_roughness_Fig6_8, 'k-', DisplayName='ML model', LineWidth=1.5)
hold on;
plot(Xauditory_roughness_Fig6_8, acoustic_roughness_auditory_roughness_Fig6_8, 'b--+', DisplayName='standardized model', LineWidth=1)
plot([40 60 80], [34.7 54.7 100], 'b--o', DisplayName='subjective', LineWidth=1.5);
window = legend('show','Location','northwest');
set(window, 'Interpreter','latex')
xlabel('SPL\,[dB]', 'Interpreter','latex','FontSize',16);
ylabel('$R/R_{0}$\,[\%]', 'Interpreter','latex','FontSize',16);
xlim("padded");
set(gca,'FontSize',16,'FontName',  'Times New Roman');
exportgraphics(gca, '../../roughness_research/master_thesis/result_rnn/SPL_AM.pdf' );
% exportgraphics(gca, '../../roughness_research/master_thesis/result_cnn/SPL_AM.pdf' );
%% Test roughness in dependence on modulation frequency in SFM tone
% VENCOVSKY Fig.11

N = height(struct2table(files7));
 
YPred_VENCOVSKY_Fig11 = zeros(1, N);
acoustic_roughness_VENCOVSKY_Fig11 = zeros(1, N);

for n=1:N
    fileName = append('audio_SFM_modulation_frequency/audio', int2str(n), '.wav');
    [x, Fs] = audioread(fileName);
    YPred_VENCOVSKY_Fig11(n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    roughness = median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]);
    acoustic_roughness_VENCOVSKY_Fig11(n) = mean(roughness);
end

XVENCOVSKY_Fig11 = 10:300;

% subjective data
T = readtable('roughness_test_sub_data.xlsx');
y_sub_VENCOVSKY_Fig11 = table2array(T(189:199,2));
x_sub_VENCOVSKY_Fig11 = table2array(T(189:199,3));
% error bar
err_neg = table2array(T(189:199,12));
err_pos = table2array(T(189:199,11));

figure('Name','VENCOVSKY Fig.11','NumberTitle','off');

semilogx(XVENCOVSKY_Fig11, YPred_VENCOVSKY_Fig11, 'k-', DisplayName='ML model', LineWidth=1.5)
hold on;
semilogx(XVENCOVSKY_Fig11, acoustic_roughness_VENCOVSKY_Fig11, 'b--+', DisplayName='ISO 532-1', LineWidth=1.5)
errorbar(x_sub_VENCOVSKY_Fig11, y_sub_VENCOVSKY_Fig11, err_neg, err_pos, 'b--o', DisplayName='subjective');
legend('show');
title('dependence on modulation frequency in SFM tone');
xlabel('modulation frequency');
ylabel('roughness [asper]');

%% Test roughness in dependence on frequency division in SFM tone
% VENCOVSKY Fig.12

N = height(struct2table(files8));
 
YPred_VENCOVSKY_Fig12 = zeros(1, N);
acoustic_roughness_VENCOVSKY_Fig12 = zeros(1, N);

for n=1:N
    fileName = append('audio_SFM_frequency_division/audio', int2str(n), '.wav');
    [x, Fs] = audioread(fileName);
    YPred_VENCOVSKY_Fig12(n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    roughness = median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]);
    acoustic_roughness_VENCOVSKY_Fig12(n) = mean(roughness);
end

XVENCOVSKY_Fig12 = linspace(64,1624,781);

% subjective data
T = readtable('roughness_test_sub_data.xlsx');
y_sub_VENCOVSKY_Fig12 = table2array(T(200:209,2));
x_sub_VENCOVSKY_Fig12 = table2array(T(200:209,3));
% error bar
err_neg = table2array(T(200:209,12));
err_pos = table2array(T(200:209,11));

figure('Name','VENCOVSKY Fig.12','NumberTitle','off');

semilogx(XVENCOVSKY_Fig12, YPred_VENCOVSKY_Fig12, 'k-', DisplayName='ML model', LineWidth=1.5)
hold on;
semilogx(XVENCOVSKY_Fig12, acoustic_roughness_VENCOVSKY_Fig12, 'b--+', DisplayName='ISO 532-1', LineWidth=1.5)
errorbar(x_sub_VENCOVSKY_Fig12, y_sub_VENCOVSKY_Fig12, err_neg, err_pos, 'b--o', DisplayName='subjective');
legend('show');
title('dependence on frequency division in SFM tone');
xlabel('frequency division');
ylabel('roughness [asper]');

%% Test roughness in musical scale
% VENCOVSKY Fig.14

N = height(struct2table(files9));
 
YPred_VENCOVSKY_Fig14 = zeros(1, N);
acoustic_roughness_VENCOVSKY_Fig14 = zeros(1, N);

for n=1:N
    fileName = append('audio_musical_scale/audio', int2str(n), '.wav');
    [x, Fs] = audioread(fileName);
    if rem(n,10) == 1
        sound(x(1:9600),Fs);
    end
    YPred_VENCOVSKY_Fig14(n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    roughness = median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]);
    acoustic_roughness_VENCOVSKY_Fig14(n) = mean(roughness);
end

XVENCOVSKY_Fig14 = 1:121;

% subjective data
T = readtable('roughness_test_sub_data.xlsx');
y_sub_VENCOVSKY_Fig14 = table2array(T(210:222,2));

% error bar
err_neg = table2array(T(210:222,12));
err_pos = table2array(T(210:222,11));

figure('Name','VENCOVSKY Fig.14','NumberTitle','off');

plot(XVENCOVSKY_Fig14, YPred_VENCOVSKY_Fig14, 'k-', DisplayName='ML model', LineWidth=1.5)
hold on;
plot(XVENCOVSKY_Fig14, acoustic_roughness_VENCOVSKY_Fig14, 'b--+', DisplayName='ISO 532-1', LineWidth=1.5)
xticks([1 11 21 31 41 51 61 71 81 91 101 111 121]);
xticklabels({'C4', 'C#4', 'D4', 'D#4', 'E4', 'E#4', 'F4', 'G4', 'G#4', 'A4', 'A#4', 'B4', 'C5'});
errorbar([1 11 21 31 41 51 61 71 81 91 101 111 121], y_sub_VENCOVSKY_Fig14, err_neg, err_pos, 'b--o', DisplayName='subjective');
legend('show');
title('dependence on musical scale');
xlabel('Upper note of harmonic interval');
ylabel('roughness [asper]');

%% Test roughness in dependence on frequency division in SFM tone
% ch11.5
%fc=1500, fm=70, 70dB

N = 1221;
 
YPred_ch11 = zeros(1, N);
acoustic_roughness_ch11 = zeros(1, N);

for n=1:N
    fileName = append('audio_SFM_frequency_division_ch11/audio', int2str(n), '.wav');
    [x, Fs] = audioread(fileName);
    YPred_ch11(n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    roughness = median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]);
    acoustic_roughness_ch11(n) = mean(roughness);
end

X_ch11 = 30:1250;
% X_ch11 = [30 64 120 160 315 555 790 1250];

% % subjective data
% T = readtable('roughness_test_sub_data.xlsx');
% y_sub_VENCOVSKY_Fig12 = table2array(T(200:209,2));
% x_sub_VENCOVSKY_Fig12 = table2array(T(200:209,3));
% % error bar
% err_neg = table2array(T(200:209,12));
% err_pos = table2array(T(200:209,11));

figure('Name','ch11','NumberTitle','off');

semilogx(X_ch11, YPred_ch11, 'k-', DisplayName='ML model', LineWidth=1.5)
hold on;
semilogx(X_ch11, acoustic_roughness_ch11, 'b--+', DisplayName='ISO 532-1', LineWidth=1.5)
xlim([20 2000])
xticks([20 50 100 200 500 1000 2000]);
xticklabels({'20', '50', '100', '200', '500', '1000', '2000'});
% errorbar(x_sub_VENCOVSKY_Fig12, y_sub_VENCOVSKY_Fig12, err_neg, err_pos, 'b--o', DisplayName='subjective');
legend('show');
title('dependence on frequency division in SFM tone');
xlabel('frequency division');
ylabel('roughness [asper]');

%% Test roughness in dependence on  modulation frequency in SAM tone
% Fastl figure1

N = 7;
 
YPred_fastl_a = zeros(1, N);
YPred_fastl_b = zeros(1, N);
YPred_fastl_c = zeros(1, N);
acoustic_roughness_fastl_a = zeros(1, N);
acoustic_roughness_fastl_b = zeros(1, N);
acoustic_roughness_fastl_c = zeros(1, N);

for n=1:N
    fileName = append('audio_fastl_a/audio', int2str(n), '.wav');
    [x, Fs] = audioread(fileName);
    YPred_fastl_a(n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).') predict(net, x(19201:28800).')]);
    roughness = median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]);
    acoustic_roughness_fastl_a(n) = mean(roughness);
end

for n=1:N
    fileName = append('audio_fastl_b/audio', int2str(n), '.wav');
    [x, Fs] = audioread(fileName);
    YPred_fastl_b(n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    roughness = median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]);
    acoustic_roughness_fastl_b(n) = mean(roughness);
end

for n=1:N
    fileName = append('audio_fastl_c/audio', int2str(n), '.wav');
    [x, Fs] = audioread(fileName);
    YPred_fastl_c(n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    roughness = median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]);
    acoustic_roughness_fastl_c(n) = mean(roughness);
end


X_fastl = [10 20 50 70 100 200 500];

figure('Name','fastl','NumberTitle','off');

subplot(1,3,1);
semilogx(X_fastl, YPred_fastl_a, 'k-', DisplayName='ML model', LineWidth=1.5)
hold on;
semilogx(X_fastl, acoustic_roughness_fastl_a, 'b--+', DisplayName='ISO 532-1', LineWidth=1.5)
xticks([10 20 50 100 200 500]);
xticklabels({'10', '20', '50', '100', '200', '500'});
legend('show');
title('a');
xlabel('modulation frequency');
ylabel('roughness [asper]');

subplot(1,3,2);
semilogx(X_fastl, YPred_fastl_b, 'k-', DisplayName='ML model', LineWidth=1.5)
hold on;
semilogx(X_fastl, acoustic_roughness_fastl_b, 'b--+', DisplayName='ISO 532-1', LineWidth=1.5)
xticks([10 20 50 100 200 500]);
xticklabels({'10', '20', '50', '100', '200', '500'});
title('b');
xlabel('modulation frequency');
ylabel('roughness [asper]');

subplot(1,3,3);
semilogx(X_fastl, YPred_fastl_c, 'k-', DisplayName='ML model', LineWidth=1.5)
hold on;
semilogx(X_fastl, acoustic_roughness_fastl_c, 'b--+', DisplayName='ISO 532-1', LineWidth=1.5)
xticks([10 20 50 100 200 500]);
xticklabels({'10', '20', '50', '100', '200', '500'});
title('c');
xlabel('modulation frequency');
ylabel('roughness [asper]');

%% Test roughness in sawtooth
% pressnitzer figure8

N = 9;
 
YPred_pressnitzer = zeros(1, N);
YPred_pressnitzer_r = zeros(1, N);
acoustic_roughness_pressnitzer = zeros(1, N);
acoustic_roughness_pressnitzer_r = zeros(1, N);

for n=1:N
    fileName = append('audio_sawtooth/audio', int2str(n), '.wav');
    [x, Fs] = audioread(fileName);
    YPred_pressnitzer(n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    roughness = median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]);
    acoustic_roughness_pressnitzer(n) = mean(roughness);
end

for n=1:N
    fileName = append('audio_sawtooth_reverse/audio', int2str(n), '.wav');
    [x, Fs] = audioread(fileName);
    YPred_pressnitzer_r(n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    roughness = median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]);
    acoustic_roughness_pressnitzer_r(n) = mean(roughness);
end



X_pressnitzer = [0.4 0.6 0.8];

figure('Name','pressnitzer 1999','NumberTitle','off');

subplot(1,3,1);
plot(X_pressnitzer, YPred_pressnitzer(1:3), 'k--+', DisplayName='Sawtooth (ML model)', LineWidth=1.5)
hold on;
plot(X_pressnitzer, YPred_pressnitzer_r(1:3), 'k-+', DisplayName='Reversed (ML model)', LineWidth=1.5)
plot(X_pressnitzer, acoustic_roughness_pressnitzer(1:3), 'b--+', DisplayName='Sawtooth (ISO 532-1)', LineWidth=1.5)
plot(X_pressnitzer, acoustic_roughness_pressnitzer_r(1:3), 'b-+', DisplayName='Reversed (ISO 532-1)', LineWidth=1.5)
title('fc=2500, fm=70');
xlabel('modulation depth');
ylabel('roughness [asper]');

subplot(1,3,2);
plot(X_pressnitzer, YPred_pressnitzer(4:6), 'k--+', DisplayName='Sawtooth (ML model)', LineWidth=1.5)
hold on;
plot(X_pressnitzer, YPred_pressnitzer_r(4:6), 'k-+', DisplayName='Reversed (ML model)', LineWidth=1.5)
plot(X_pressnitzer, acoustic_roughness_pressnitzer(4:6), 'b--+', DisplayName='Sawtooth (ISO 532-1)', LineWidth=1.5)
plot(X_pressnitzer, acoustic_roughness_pressnitzer_r(4:6), 'b-+', DisplayName='Reversed (ISO 532-1)', LineWidth=1.5)
legend('show');
title('fc=5000, fm=70');
xlabel('modulation depth');
ylabel('roughness [asper]');

subplot(1,3,3);
plot(X_pressnitzer, YPred_pressnitzer(7:9), 'k--+', DisplayName='Sawtooth (ML model)', LineWidth=1.5)
hold on;
plot(X_pressnitzer, YPred_pressnitzer_r(7:9), 'k-+', DisplayName='Reversed (ML model)', LineWidth=1.5)
plot(X_pressnitzer, acoustic_roughness_pressnitzer(7:9), 'b--+', DisplayName='Sawtooth (ISO 532-1)', LineWidth=1.5)
plot(X_pressnitzer, acoustic_roughness_pressnitzer_r(7:9), 'b-+', DisplayName='Reversed (ISO 532-1)', LineWidth=1.5)
title('fc=10000, fm=70');
xlabel('modulation depth');
ylabel('roughness [asper]');
%% Test roughness in dependence on phase in pAM tone
% Pressnitzer&McAdams1999 figure 2, 3

N = 4;

% fc=125, fmod=30, 60dB

XPhase = [0 pi/6 pi/3 pi/2];
YPredPhase_125_p = zeros(1, N);
YPredPhase_125_n = zeros(1, N);
acoustic_roughness_Phase_125_p = zeros(1, N);
acoustic_roughness_Phase_125_n = zeros(1, N);

for n=1:4
    fileName_p = append('audio/audio_pSAM_phase/audio', int2str(n), '.wav');
    if n == 1
        fileName_n = fileName_p;
    else
        fileName_n = append('audio/audio_pSAM_phase/audio', int2str(9-n), '.wav');
    end
    [x, Fs] = audioread(fileName_p);
    YPredPhase_125_p(n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    acoustic_roughness_Phase_125_p(n) = mean(median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]));
    [x, Fs] = audioread(fileName_n);
    YPredPhase_125_n(n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    acoustic_roughness_Phase_125_n(n) = mean(median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]));
end

% fc=250, fmod=40, 60dB

YPredPhase_250_p = zeros(1, N);
YPredPhase_250_n = zeros(1, N);
acoustic_roughness_Phase_250_p = zeros(1, N);
acoustic_roughness_Phase_250_n = zeros(1, N);

for n=1:4
    fileName_p = append('audio/audio_pSAM_phase/audio', int2str(n+7), '.wav');
    if n == 1
        fileName_n = fileName_p;
    else
        fileName_n = append('audio/audio_pSAM_phase/audio', int2str(16-n), '.wav');
    end
    [x, Fs] = audioread(fileName_p);
    YPredPhase_250_p(n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    acoustic_roughness_Phase_250_p(n) = mean(median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]));
    [x, Fs] = audioread(fileName_n);
    YPredPhase_250_n(n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    acoustic_roughness_Phase_250_n(n) = mean(median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]));
end

% fc=500, fmod=50, 60dB

YPredPhase_500_p = zeros(1, N);
YPredPhase_500_n = zeros(1, N);
acoustic_roughness_Phase_500_p = zeros(1, N);
acoustic_roughness_Phase_500_n = zeros(1, N);

for n=1:4
    fileName_p = append('audio/audio_pSAM_phase/audio', int2str(n+14), '.wav');
    if n == 1
        fileName_n = fileName_p;
    else
        fileName_n = append('audio/audio_pSAM_phase/audio', int2str(23-n), '.wav');
    end
    [x, Fs] = audioread(fileName_p);
    YPredPhase_500_p(n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    acoustic_roughness_Phase_500_p(n) = mean(median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]));
    [x, Fs] = audioread(fileName_n);
    YPredPhase_500_n(n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    acoustic_roughness_Phase_500_n(n) = mean(median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]));
end

% fc=1000, fmod=70, 60dB

YPredPhase_1000_p = zeros(1, N);
YPredPhase_1000_n = zeros(1, N);
acoustic_roughness_Phase_1000_p = zeros(1, N);
acoustic_roughness_Phase_1000_n = zeros(1, N);

for n=1:4
    fileName_p = append('audio/audio_pSAM_phase/audio', int2str(n+21), '.wav');
    if n == 1
        fileName_n = fileName_p;
    else
        fileName_n = append('audio/audio_pSAM_phase/audio', int2str(30-n), '.wav');
    end
    [x, Fs] = audioread(fileName_p);
    YPredPhase_1000_p(n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    acoustic_roughness_Phase_1000_p(n) = mean(median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]));
    [x, Fs] = audioread(fileName_n);
    YPredPhase_1000_n(n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    acoustic_roughness_Phase_1000_n(n) = mean(median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]));
end

% fc=2000, fmod=70, 60dB

YPredPhase_2000_p = zeros(1, N);
YPredPhase_2000_n = zeros(1, N);
acoustic_roughness_Phase_2000_p = zeros(1, N);
acoustic_roughness_Phase_2000_n = zeros(1, N);

for n=1:4
    fileName_p = append('audio/audio_pSAM_phase/audio', int2str(n+28), '.wav');
    if n == 1
        fileName_n = fileName_p;
    else
        fileName_n = append('audio/audio_pSAM_phase/audio', int2str(37-n), '.wav');
    end
    [x, Fs] = audioread(fileName_p);
    YPredPhase_2000_p(n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    acoustic_roughness_Phase_2000_p(n) = mean(median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]));
    [x, Fs] = audioread(fileName_n);
    YPredPhase_2000_n(n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    acoustic_roughness_Phase_2000_n(n) = mean(median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]));
end

% fc=4000, fmod=70, 60dB

YPredPhase_4000_p = zeros(1, N);
YPredPhase_4000_n = zeros(1, N);
acoustic_roughness_Phase_4000_p = zeros(1, N);
acoustic_roughness_Phase_4000_n = zeros(1, N);

for n=1:4
    fileName_p = append('audio/audio_pSAM_phase/audio', int2str(n+35), '.wav');
    if n == 1
        fileName_n = fileName_p;
    else
        fileName_n = append('audio/audio_pSAM_phase/audio', int2str(44-n), '.wav');
    end
    [x, Fs] = audioread(fileName_p);
    YPredPhase_4000_p(n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    acoustic_roughness_Phase_4000_p(n) = mean(median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]));
    [x, Fs] = audioread(fileName_n);
    YPredPhase_4000_n(n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    acoustic_roughness_Phase_4000_n(n) = mean(median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]));
end

% fc=8000, fmod=70, 60dB

YPredPhase_8000_p = zeros(1, N);
YPredPhase_8000_n = zeros(1, N);
acoustic_roughness_Phase_8000_p = zeros(1, N);
acoustic_roughness_Phase_8000_n = zeros(1, N);

for n=1:4
    fileName_p = append('audio/audio_pSAM_phase/audio', int2str(n+42), '.wav');
    if n == 1
        fileName_n = fileName_p;
    else
        fileName_n = append('audio/audio_pSAM_phase/audio', int2str(51-n), '.wav');
    end
    [x, Fs] = audioread(fileName_p);
    YPredPhase_8000_p(n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    acoustic_roughness_Phase_8000_p(n) = mean(median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]));
    [x, Fs] = audioread(fileName_n);
    YPredPhase_8000_n(n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    acoustic_roughness_Phase_8000_n(n) = mean(median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]));
end

T = readtable('roughness_test_sub_data.xlsx');
Y_PredPhase_125_p = table2array(T(228:231,2));
Y_PredPhase_125_n = table2array(T(232:235,2));
Y_PredPhase_250_p = table2array(T(236:239,2));
Y_PredPhase_250_n = table2array(T(240:243,2));
Y_PredPhase_500_p = table2array(T(244:247,2));
Y_PredPhase_500_n = table2array(T(248:251,2));
Y_PredPhase_1000_p = table2array(T(252:255,2));
Y_PredPhase_1000_n = table2array(T(256:259,2));
Y_PredPhase_2000_p = table2array(T(260:263,2));
Y_PredPhase_2000_n = table2array(T(264:267,2));
Y_PredPhase_4000_p = table2array(T(268:271,2));
Y_PredPhase_4000_n = table2array(T(272:275,2));
Y_PredPhase_8000_p = table2array(T(276:279,2));
Y_PredPhase_8000_n = table2array(T(280:283,2));

figure('Position',[0 0 600 830]);
t = tiledlayout(4,2,'TileSpacing','Compact','Padding','Compact');

t1 = nexttile;
plot(XPhase, YPredPhase_125_p, 'k-', DisplayName='ML model : $\Phi$\,$>$\,$0$', Linewidth=1);
hold on;
plot(XPhase, YPredPhase_125_n, 'k--', DisplayName='ML model : $\Phi$\,$<$\,$0$', Linewidth=1);
plot(XPhase, acoustic_roughness_Phase_125_p, 'b-+', DisplayName='standardized model : $\Phi$\,$>$\,$0$', Linewidth=1);
plot(XPhase, acoustic_roughness_Phase_125_n, 'b--+', DisplayName='standardized model : $\Phi$\,$<$\,$0$', Linewidth=1);
plot(XPhase, Y_PredPhase_125_p, 'b-o', DisplayName='calculated : $\Phi$\,$>$\,$0$', Linewidth=1);
plot(XPhase, Y_PredPhase_125_n, 'b--o', DisplayName='calculated : $\Phi$\,$<$\,$0$', Linewidth=1);
window  = legend('show','Location','northeast');
set(window, 'Interpreter','latex')
xticks([0 pi/6 pi/3 pi/2]);
xticklabels({'0', '$\pi$/6', '$\pi$/3', '$\pi$/2'});
xlim("padded");
ylim([0 1.2]);
title(t1,'$f_{c}$=$125$\,Hz, $f_{mod}$=$30$\,Hz','Interpreter','latex','FontSize',16);
set(gca,'FontSize',15,'FontName',  'Times New Roman');


t2 = nexttile;
plot(XPhase, YPredPhase_250_p, 'k-', 'DisplayName', 'ML model : Φ > 0', Linewidth=1);
hold on;
plot(XPhase, YPredPhase_250_n, 'k--', 'DisplayName', 'ML model : Φ < 0', Linewidth=1);
plot(XPhase, acoustic_roughness_Phase_250_p, 'b-+', 'DisplayName', 'standardized model : Φ > 0', Linewidth=1);
plot(XPhase, acoustic_roughness_Phase_250_n, 'b--+', 'DisplayName', 'standardized model : Φ < 0', Linewidth=1);
plot(XPhase, Y_PredPhase_250_p, 'b-o', 'DisplayName', 'subjective : Φ > 0', Linewidth=1);
plot(XPhase, Y_PredPhase_250_n, 'b--o', 'DisplayName', 'subjective : Φ < 0', Linewidth=1);
xticks([0 pi/6 pi/3 pi/2]);
xticklabels({'0', '$\pi$/6', '$\pi$/3', '$\pi$/2'});
xlim("padded");
ylim([0 1.2]);
title(t2,'$f_{c}$=$250$\,Hz, $f_{mod}$=$40$\,Hz','Interpreter','latex','FontSize',16);
set(gca,'FontSize',15,'FontName',  'Times New Roman');

t3 = nexttile;
plot(XPhase, YPredPhase_500_p, 'k-', 'DisplayName', 'ML model : Φ > 0', LineWidth=1);
hold on;
plot(XPhase, YPredPhase_500_n, 'k--', 'DisplayName', 'ML model : Φ < 0', LineWidth=1);
plot(XPhase, acoustic_roughness_Phase_500_p, 'b-+', 'DisplayName', 'standardized model : Φ > 0', Linewidth=1);
plot(XPhase, acoustic_roughness_Phase_500_n, 'b--+', 'DisplayName', 'standardized model : Φ < 0', Linewidth=1);
plot(XPhase, Y_PredPhase_500_p, 'b-o', 'DisplayName', 'subjective : Φ > 0', Linewidth=1);
plot(XPhase, Y_PredPhase_500_n, 'b--o', 'DisplayName', 'subjective : Φ < 0', Linewidth=1);
xticks([0 pi/6 pi/3 pi/2]);
xticklabels({'0', '$\pi$/6', '$\pi$/3', '$\pi$/2'});
xlim("padded");
ylim([0 1.2]);
title(t3,'$f_{c}$=$500$\,Hz, $f_{mod}$=$50$\,Hz','Interpreter','latex','FontSize',16);
set(gca,'FontSize',15,'FontName',  'Times New Roman');

t4 = nexttile;
plot(XPhase, YPredPhase_1000_p, 'k-', 'DisplayName', 'ML model : Φ > 0', Linewidth=1);
hold on;
plot(XPhase, YPredPhase_1000_n, 'k--', 'DisplayName', 'ML model : Φ < 0', Linewidth=1);
plot(XPhase, acoustic_roughness_Phase_1000_p, 'b-+', 'DisplayName', 'standardized model: Φ > 0', Linewidth=1);
plot(XPhase, acoustic_roughness_Phase_1000_n, 'b--+', 'DisplayName', 'standardized model: Φ < 0', Linewidth=1);
plot(XPhase, Y_PredPhase_1000_p, 'b-o', 'DisplayName', 'subjective : Φ > 0', Linewidth=1);
plot(XPhase, Y_PredPhase_1000_n, 'b--o', 'DisplayName', 'subjective : Φ < 0', Linewidth=1);
xticks([0 pi/6 pi/3 pi/2]);
xticklabels({'0', '$\pi$/6', '$\pi$/3', '$\pi$/2'});
xlim("padded");
ylim([0 1.2]);
title(t4,'$f_{c}$=$1$\,kHz, $f_{mod}$=$70$\,Hz','Interpreter','latex','FontSize',16);
set(gca,'FontSize',15,'FontName',  'Times New Roman');

t5 = nexttile;
plot(XPhase, YPredPhase_2000_p, 'k-', 'DisplayName', 'ML model : Φ > 0', LineWidth=1);
hold on;
plot(XPhase, YPredPhase_2000_n, 'k--', 'DisplayName', 'ML model : Φ < 0', LineWidth=1);
plot(XPhase, acoustic_roughness_Phase_2000_p, 'b-+', 'DisplayName', 'calculated : Φ > 0', Linewidth=1);
plot(XPhase, acoustic_roughness_Phase_2000_n, 'b--+', 'DisplayName', 'calculated : Φ < 0', Linewidth=1);
plot(XPhase, Y_PredPhase_2000_p, 'b-o', 'DisplayName', 'subjective : Φ > 0', Linewidth=1);
plot(XPhase, Y_PredPhase_2000_n, 'b--o', 'DisplayName', 'subjective : Φ < 0', Linewidth=1);
xticks([0 pi/6 pi/3 pi/2]);
xticklabels({'0', '$\pi$/6', '$\pi$/3', '$\pi$/2'});
xlim("padded");
ylim([0 1.2]);
title(t5,'$f_{c}$=$2$\,kHz, $f_{mod}$=$70$\,Hz','Interpreter','latex','FontSize',16);
set(gca,'FontSize',15,'FontName',  'Times New Roman');

t6 = nexttile;
plot(XPhase, YPredPhase_4000_p, 'k-', 'DisplayName', 'ML model : Φ > 0', Linewidth=1);
hold on;
plot(XPhase, YPredPhase_4000_n, 'k--', 'DisplayName', 'ML model : Φ < 0', Linewidth=1);
plot(XPhase, acoustic_roughness_Phase_4000_p, 'b-+', 'DisplayName', 'standardized model: Φ > 0', Linewidth=1);
plot(XPhase, acoustic_roughness_Phase_4000_n, 'b--+', 'DisplayName', 'standardized model: Φ < 0', Linewidth=1);
plot(XPhase, Y_PredPhase_4000_p, 'b-o', 'DisplayName', 'subjective : Φ > 0', Linewidth=1);
plot(XPhase, Y_PredPhase_4000_n, 'b--o', 'DisplayName', 'subjective : Φ < 0', Linewidth=1);
xticks([0 pi/6 pi/3 pi/2]);
xticklabels({'0', '$\pi$/6', '$\pi$/3', '$\pi$/2'});
xlim("padded");
ylim([0 1.2]);
title(t6,'$f_{c}$=$4$\,kHz, $f_{mod}$=$70$\,Hz','Interpreter','latex','FontSize',15);
set(gca,'FontSize',15,'FontName',  'Times New Roman');

set(groot, 'defaultAxesTickLabelInterpreter' , 'latex' ); 
t7 = nexttile;
plot(XPhase, YPredPhase_8000_p, 'k-', 'DisplayName', 'ML model: \Phi > 0', LineWidth=1);
hold on;
plot(XPhase, YPredPhase_8000_n, 'k--', 'DisplayName', 'ML model: \Phi < 0', LineWidth=1);
plot(XPhase, acoustic_roughness_Phase_8000_p, 'b-+', 'DisplayName', 'standardized model: \Phi > 0', Linewidth=1);
plot(XPhase, acoustic_roughness_Phase_8000_n, 'b--+', 'DisplayName', 'standardized model: \Phi < 0', Linewidth=1);
plot(XPhase, Y_PredPhase_8000_p, 'b-o', 'DisplayName', 'calculated : \Phi > 0', Linewidth=1);
plot(XPhase, Y_PredPhase_8000_n, 'b--o', 'DisplayName', 'calculated : \Phi < 0', Linewidth=1);
xticks([0 pi/6 pi/3 pi/2]);
xticklabels({'0', '$\pi$/6', '$\pi$/3', '$\pi$/2'});
xlim("padded");
ylim([0 1.2]);
title(t7,'$f_{c}$=$8$\,kHz, $f_{mod}$=$70$\,Hz','Interpreter','latex','FontSize',16);
set(gca,'FontSize',15,'FontName',  'Times New Roman');

xlabel(t,'$|\Phi|$','Interpreter','latex','FontSize',16);
ylabel(t,'$R$\,[asper]','Interpreter','latex','FontSize',16);
