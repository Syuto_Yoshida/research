%%
T = readtable('roughness_data/roughness_training_data_SAM_modulation_frequency.xlsx');
y_sub_DanielAndWeber_Fig3_125 = table2array(T(1:101,2));
x_sub_DanielAndWeber_Fig3_125 = 0:100;

y_sub_DanielAndWeber_Fig3_500 = table2array(T(102:302,2));
x_sub_DanielAndWeber_Fig3_500 = 0:200;

y_sub_DanielAndWeber_Fig3_8000 = table2array(T(303:603,2));
x_sub_DanielAndWeber_Fig3_8000 = 0:300;

y_sub_DanielAndWeber_Fig3_2000 = table2array(T(604:904,2));
x_sub_DanielAndWeber_Fig3_2000 = 0:300;

y_sub_DanielAndWeber_Fig3_1000 = table2array(T(905:1205,2));
x_sub_DanielAndWeber_Fig3_1000 = 0:300;

y_sub_DanielAndWeber_Fig3_4000 = table2array(T(1206:1506,2));
x_sub_DanielAndWeber_Fig3_4000 = 0:300;

y_sub_DanielAndWeber_Fig3_250 = table2array(T(1507:1707,2));
x_sub_DanielAndWeber_Fig3_250 = 0:200;


T = readtable('roughness_test_sub_data.xlsx');

%　Data preparation 
y_sub_DanielAndWeber_Fig3_125_line = table2array(T(1:9,2));
x_sub_DanielAndWeber_Fig3_125_line = linspace(20, 100, 9);

y_sub_DanielAndWeber_Fig3_500_line = table2array(T(10:24,2));
x_sub_DanielAndWeber_Fig3_500_line = linspace(20, 160, 15);

y_sub_DanielAndWeber_Fig3_8000_line = table2array(T(25:39,2));
x_sub_DanielAndWeber_Fig3_8000_line = linspace(20, 160, 15);

y_sub_DanielAndWeber_Fig3_2000_line = table2array(T(40:54,2));
x_sub_DanielAndWeber_Fig3_2000_line = linspace(20, 160, 15);

y_sub_DanielAndWeber_Fig3_1000_line = table2array(T(55:69,2));
x_sub_DanielAndWeber_Fig3_1000_line = linspace(20, 160, 15);

y_sub_DanielAndWeber_Fig3_4000_line = table2array(T(70:84,2));
x_sub_DanielAndWeber_Fig3_4000_line = linspace(20, 160, 15);

y_sub_DanielAndWeber_Fig3_250_line = table2array(T(85:96,2));
x_sub_DanielAndWeber_Fig3_250_line = linspace(20, 130, 12);
figure('Name','Checking data of Daniel and Weber Fig.3','NumberTitle','off');

subplot(2,4,1); 
plot(x_sub_DanielAndWeber_Fig3_125, y_sub_DanielAndWeber_Fig3_125, 'b:', DisplayName='model', LineWidth=1.5)
hold on;
plot(x_sub_DanielAndWeber_Fig3_125_line, y_sub_DanielAndWeber_Fig3_125_line, 'b--o', DisplayName='subjective');
title('125 Hz');
xlabel('fmod [Hz]');
ylabel('Subjective Roughness [asper]');

subplot(2,4,2); 
plot(x_sub_DanielAndWeber_Fig3_500, y_sub_DanielAndWeber_Fig3_500, 'b:', DisplayName='model', LineWidth=1.5)
hold on;
plot(x_sub_DanielAndWeber_Fig3_500_line, y_sub_DanielAndWeber_Fig3_500_line, 'b--o', DisplayName='subjective');
title('500 Hz');
xlabel('fmod [Hz]');
ylabel('Subjective Roughness [asper]');

subplot(2,4,3); 
plot(x_sub_DanielAndWeber_Fig3_8000, y_sub_DanielAndWeber_Fig3_8000, 'b:', DisplayName='model', LineWidth=1.5)
hold on;
plot(x_sub_DanielAndWeber_Fig3_8000_line, y_sub_DanielAndWeber_Fig3_8000_line, 'b--o', DisplayName='subjective');
title('8000 Hz');
xlabel('fmod [Hz]');
ylabel('Subjective Roughness [asper]');

subplot(2,4,4); 
plot(x_sub_DanielAndWeber_Fig3_2000, y_sub_DanielAndWeber_Fig3_2000, 'b:', DisplayName='model', LineWidth=1.5)
hold on;
plot(x_sub_DanielAndWeber_Fig3_2000_line, y_sub_DanielAndWeber_Fig3_2000_line, 'b--o', DisplayName='subjective');
title('2000 Hz');
xlabel('fmod [Hz]');
ylabel('Subjective Roughness [asper]');

subplot(2,4,5); 
plot(x_sub_DanielAndWeber_Fig3_1000, y_sub_DanielAndWeber_Fig3_1000, 'b:', DisplayName='model', LineWidth=1.5)
hold on;
plot(x_sub_DanielAndWeber_Fig3_1000_line, y_sub_DanielAndWeber_Fig3_1000_line, 'b--o', DisplayName='subjective');
title('1000 Hz');
xlabel('fmod [Hz]');
ylabel('Subjective Roughness [asper]');

subplot(2,4,6); 
plot(x_sub_DanielAndWeber_Fig3_4000, y_sub_DanielAndWeber_Fig3_4000, 'b:', DisplayName='model', LineWidth=1.5)
hold on;
plot(x_sub_DanielAndWeber_Fig3_4000_line, y_sub_DanielAndWeber_Fig3_4000_line, 'b--o', DisplayName='subjective');
title('4000 Hz');
xlabel('fmod [Hz]');
ylabel('Subjective Roughness [asper]');

subplot(2,4,7); 
plot(x_sub_DanielAndWeber_Fig3_250, y_sub_DanielAndWeber_Fig3_250, 'b:', DisplayName='model', LineWidth=1.5)
hold on;
plot(x_sub_DanielAndWeber_Fig3_250_line, y_sub_DanielAndWeber_Fig3_250_line, 'b--o', DisplayName='subjective');
title('250 Hz');
xlabel('fmod [Hz]');
ylabel('Subjective Roughness [asper]');

%%
figure('Name','Daniel and Weber Fig.4, 5, 6, 7','NumberTitle','off');

T = readtable('roughness_data/roughness_training_data_SAM_modulation_frequency.xlsx');

y_sub_DanielAndWeber_Fig4_250 = table2array(T(1708:1878,2));
x_sub_DanielAndWeber_Fig4 = linspace(15,32,171);

subplot(2,4,1);
plot(x_sub_DanielAndWeber_Fig4, y_sub_DanielAndWeber_Fig4_250, '--o', DisplayName='model', LineWidth=1.5)
title('Fig4 250Hz');
xlabel('fmod');
ylabel('Subjective Roughness [asper]');

y_sub_DanielAndWeber_Fig4_500 = table2array(T(1879:2049,2));

subplot(2,4,2);
plot(x_sub_DanielAndWeber_Fig4, y_sub_DanielAndWeber_Fig4_500, '--o', DisplayName='model', LineWidth=1.5)
title('Fig4 500Hz');
xlabel('fmod');
ylabel('Subjective Roughness [asper]');

y_sub_DanielAndWeber_Fig4_1000 = table2array(T(2050:2220,2));

subplot(2,4,3);
plot(x_sub_DanielAndWeber_Fig4, y_sub_DanielAndWeber_Fig4_1000, '--o', DisplayName='model', LineWidth=1.5)
title('Fig4 1000Hz');
xlabel('fmod');
ylabel('Subjective Roughness [asper]');

y_sub_DanielAndWeber_Fig4_5000 = table2array(T(2221:2391,2));

subplot(2,4,4);
plot(x_sub_DanielAndWeber_Fig4, y_sub_DanielAndWeber_Fig4_5000, '--o', DisplayName='model', LineWidth=1.5)
title('Fig4 5000Hz');
xlabel('fmod');
ylabel('Subjective Roughness [asper]');

T = readtable('roughness_data/roughness_training_data_SAM_modulation_depth.xlsx');

y_sub_DanielAndWeber_Fig5 = table2array(T(1:101,2));
x_sub_DanielAndWeber_Fig5 = linspace(0, 1, 101);

subplot(2,4,5);
plot(x_sub_DanielAndWeber_Fig5, y_sub_DanielAndWeber_Fig5, 'b:', DisplayName='model', LineWidth=1.5)
title('Fig5');
xlabel('m');
ylabel('Subjective Roughness [asper]');

y_sub_DanielAndWeber_Fig6 = table2array(T(102:202,2));
x_sub_DanielAndWeber_Fig6 = linspace(0, 1, 101);

subplot(2,4,6);
plot(x_sub_DanielAndWeber_Fig6, y_sub_DanielAndWeber_Fig6, 'b:', DisplayName='model', LineWidth=1.5)
title('Fig6');
xlabel('m');
ylabel('Subjective Roughness [asper]');

y_sub_DanielAndWeber_Fig7 = table2array(T(203:443,2));
x_sub_DanielAndWeber_Fig7 = linspace(0.2, 2.6, 241);

subplot(2,4,7);
plot(x_sub_DanielAndWeber_Fig7, y_sub_DanielAndWeber_Fig7, 'b:', DisplayName='model', LineWidth=1.5)
title('Fig7');
xlabel('m');
ylabel('Subjective Roughness [asper]');

%%
T_line = readtable('roughness_test_sub_data.xlsx');
y_sub_DanielAndWeber_Fig8 = table2array(T_line(119:159,2));

figure('Name','Daniel and Weber Fig.8, 9, 10, 11','NumberTitle','off');
T = readtable('roughness_data/roughness_training_data_unmodulated_bandpass_noise.xlsx');
y_sub_DanielAndWeber_Fig8_125 = table2array(T(1:25,2));
x_sub_DanielAndWeber_Fig8_125 = linspace(10,250,25);

subplot(3,3,1);
semilogx(x_sub_DanielAndWeber_Fig8_125, y_sub_DanielAndWeber_Fig8_125, 'b:', DisplayName='model', LineWidth=1.5)
hold on;
semilogx([10 30 100 200 250], y_sub_DanielAndWeber_Fig8(1:5), 'b--o', DisplayName='subjective');
title('Fig8 125Hz');
xlabel('band with');
ylabel('Subjective Roughness [asper]');

y_sub_DanielAndWeber_Fig8_250 = table2array(T(26:75,2));
x_sub_DanielAndWeber_Fig8_250 = linspace(10,500,50);

subplot(3,3,2);
semilogx(x_sub_DanielAndWeber_Fig8_250, y_sub_DanielAndWeber_Fig8_250, 'b:', DisplayName='model', LineWidth=1.5)
hold on;
semilogx([10 30 70 100 300 500], y_sub_DanielAndWeber_Fig8(6:11), 'b--o', DisplayName='subjective');
title('Fig8 250Hz');
xlabel('band with');
ylabel('Subjective Roughness [asper]');

y_sub_DanielAndWeber_Fig8_500 = table2array(T(76:175,2));
x_sub_DanielAndWeber_Fig8_500 = linspace(10,1000,100);

subplot(3,3,3);
semilogx(x_sub_DanielAndWeber_Fig8_500, y_sub_DanielAndWeber_Fig8_500, 'b:', DisplayName='model', LineWidth=1.5)
hold on;
semilogx([10 30 70 100 300 1000], y_sub_DanielAndWeber_Fig8(12:17), 'b--o', DisplayName='subjective');
title('Fig8 500Hz');
xlabel('band with');
ylabel('Subjective Roughness [asper]');

y_sub_DanielAndWeber_Fig8_1000 = table2array(T(176:375,2));
x_sub_DanielAndWeber_Fig8_1000 = linspace(10,2000,200);

subplot(3,3,4);
semilogx(x_sub_DanielAndWeber_Fig8_1000, y_sub_DanielAndWeber_Fig8_1000, 'b:', DisplayName='model', LineWidth=1.5)
hold on;
semilogx([10 30 70 100 300 1000 2000], y_sub_DanielAndWeber_Fig8(18:24), 'b--o', DisplayName='subjective');
title('Fig8 1000Hz');
xlabel('band with');
ylabel('Subjective Roughness [asper]');

y_sub_DanielAndWeber_Fig8_2000 = table2array(T(376:775,2));
x_sub_DanielAndWeber_Fig8_2000 = linspace(10,4000,400);

subplot(3,3,5);
semilogx(x_sub_DanielAndWeber_Fig8_2000, y_sub_DanielAndWeber_Fig8_2000, 'b:', DisplayName='model', LineWidth=1.5)
hold on;
semilogx([10 30 70 100 150 300 1000 2000 4000], y_sub_DanielAndWeber_Fig8(25:33), 'b--o', DisplayName='subjective');
title('Fig8 2000Hz');
xlabel('band with');
ylabel('Subjective Roughness [asper]');

y_sub_DanielAndWeber_Fig8_4000 = table2array(T(776:1575,2));
x_sub_DanielAndWeber_Fig8_4000 = linspace(10,8000,800);

subplot(3,3,6);
semilogx(x_sub_DanielAndWeber_Fig8_4000, y_sub_DanielAndWeber_Fig8_4000, 'b:', DisplayName='model', LineWidth=1.5)
hold on;
semilogx([10 30 70 100 300 1000 2000 8000], y_sub_DanielAndWeber_Fig8(34:41), 'b--o', DisplayName='subjective');
title('Fig8 4000Hz');
xlabel('band with');
ylabel('Subjective Roughness [asper]');

T = readtable('roughness_data/roughness_training_data_FM_modulation_frequency.xlsx');
y_sub_DanielAndWeber_Fig9 = table2array(T(1:501,2)) * 100 / 3.207;
x_sub_DanielAndWeber_Fig9 = 0:500;

y_sub_DanielAndWeber_Fig9_line = table2array(T_line(160:171,2));
x_sub_DanielAndWeber_Fig9_line = [4 10 20 40 60 80 100 120 150 200 300 500];

subplot(3,3,7);
semilogx(x_sub_DanielAndWeber_Fig9, y_sub_DanielAndWeber_Fig9, 'b:', DisplayName='model', LineWidth=1.5)
hold on;
semilogx(x_sub_DanielAndWeber_Fig9_line, y_sub_DanielAndWeber_Fig9_line, 'b--o', DisplayName='subjective');
title('Fig9');
xlabel('fmod');
ylabel('Subjective Roughness [asper]');

T = readtable('roughness_data/roughness_training_data_FM_frequency_division.xlsx');
y_sub_DanielAndWeber_Fig10 = table2array(T(1:773,2)) * 100 / 3.207;
x_sub_DanielAndWeber_Fig10 = linspace(16,1560,773);

y_sub_DanielAndWeber_Fig10_line = table2array(T_line(172:183,2));
x_sub_DanielAndWeber_Fig10_line = [16 32 64 120 160 320 560 800 1000 1290 1440 1600];

subplot(3,3,8);
semilogx(x_sub_DanielAndWeber_Fig10, y_sub_DanielAndWeber_Fig10, 'b:', DisplayName='Data used for learning', LineWidth=1.5)
hold on;
semilogx(x_sub_DanielAndWeber_Fig10_line, y_sub_DanielAndWeber_Fig10_line, 'b--o', DisplayName='Data in the paper');
title('Fig10');
legend('show','Location','northwest');
xlabel('Δf');
ylabel('Subjective Roughness [asper]');

T = readtable('roughness_data/roughness_training_data_FM_dB.xlsx');
y_sub_DanielAndWeber_Fig11 = table2array(T(1:81,2));
x_sub_DanielAndWeber_Fig11 = linspace(40,80,81);

subplot(3,3,9);
plot(x_sub_DanielAndWeber_Fig11, y_sub_DanielAndWeber_Fig11, 'b:', DisplayName='model', LineWidth=1.5)
title('Fig11');
xlabel('dB');
ylabel('Subjective Roughness [asper]');

%%
figure;
T = readtable('roughness_data/roughness_training_data_musical_scale.xlsx');

y_sub_VENCOVSKY2016_Fig14 = table2array(T(1:121,2));

plot(1:121, y_sub_VENCOVSKY2016_Fig14, 'b:', DisplayName='model', LineWidth=1.5)
title('VENCOVSKY2016 Fig12');
xlabel('Δf');
xticks([1 11 21 31 41 51 61 71 81 91 101 111 121]);
xticklabels({'C4', 'C#4', 'D4', 'D#4', 'E4', 'F4', 'F#4', 'G4', 'G#4', 'A4', 'A#4', 'B4', 'C5'});
ylabel('Subjective Roughness [asper]');

%%
figure;
y_sub_auditory_roughness_Fig6_8 = table2array(T(2518:2558,2));
x_sub_auditory_roughness_Fig6_8 = 40:80;

plot(x_sub_auditory_roughness_Fig6_8, y_sub_auditory_roughness_Fig6_8, 'b:', DisplayName='model', LineWidth=1.5)
title('auditory_roughness Fig6.8');
xlabel('dB');
ylabel('Subjective Roughness [asper]');

figure;

x_sub_auditory_roughness_Fig6_10 = [0 pi/6 pi/3 pi/2];

subplot(4,2,1);
plot(x_sub_auditory_roughness_Fig6_10, table2array(T(2559:2562,2)), 'b-o', 'DisplayName', 'Φ > 0', LineWidth=1.5)
hold on;
plot([pi/6 pi/3 pi/2], flipud(table2array(T(2563:2565,2))), 'b--o', 'DisplayName', 'Φ < 0', LineWidth=1.5)
legend('show');
title('auditory_roughness Fig6.10 125Hz');
xlabel('|Φ|');
ylabel('Subjective Roughness [asper]');

subplot(4,2,2);
plot(x_sub_auditory_roughness_Fig6_10, table2array(T(2566:2569,2)), 'b-o', 'DisplayName', 'Φ > 0', LineWidth=1.5)
hold on;
plot([pi/6 pi/3 pi/2], flipud(table2array(T(2570:2572,2))), 'b--o', 'DisplayName', 'Φ < 0', LineWidth=1.5)
legend('show');
title('auditory_roughness Fig6.10 250Hz');
xlabel('|Φ|');
ylabel('Subjective Roughness [asper]');

subplot(4,2,3);
plot(x_sub_auditory_roughness_Fig6_10, table2array(T(2573:2576,2)), 'b-o', 'DisplayName', 'Φ > 0', LineWidth=1.5)
hold on;
plot([pi/6 pi/3 pi/2], flipud(table2array(T(2577:2579,2))), 'b--o', 'DisplayName', 'Φ < 0', LineWidth=1.5)
legend('show');
title('auditory_roughness Fig6.10 500Hz');
xlabel('|Φ|');
ylabel('Subjective Roughness [asper]');

subplot(4,2,4);
plot(x_sub_auditory_roughness_Fig6_10, table2array(T(2580:2583,2)), 'b-o', 'DisplayName', 'Φ > 0', LineWidth=1.5)
hold on;
plot([pi/6 pi/3 pi/2], flipud(table2array(T(2584:2586,2))), 'b--o', 'DisplayName', 'Φ < 0', LineWidth=1.5)
legend('show');
title('auditory_roughness Fig6.10 1000Hz');
xlabel('|Φ|');
ylabel('Subjective Roughness [asper]');

subplot(4,2,5);
plot(x_sub_auditory_roughness_Fig6_10, table2array(T(2587:2590,2)), 'b-o', 'DisplayName', 'Φ > 0', LineWidth=1.5)
hold on;
plot([pi/6 pi/3 pi/2], flipud(table2array(T(2591:2593,2))), 'b--o', 'DisplayName', 'Φ < 0', LineWidth=1.5)
legend('show');
title('auditory_roughness Fig6.10 2000Hz');
xlabel('|Φ|');
ylabel('Subjective Roughness [asper]');

subplot(4,2,6);
plot(x_sub_auditory_roughness_Fig6_10, table2array(T(2594:2597,2)), 'b-o', 'DisplayName', 'Φ > 0', LineWidth=1.5)
hold on;
plot([pi/6 pi/3 pi/2], flipud(table2array(T(2598:2600,2))), 'b--o', 'DisplayName', 'Φ < 0', LineWidth=1.5)
legend('show');
title('auditory_roughness Fig6.10 4000Hz');
xlabel('|Φ|');
ylabel('Subjective Roughness [asper]');

subplot(4,2,7);
plot(x_sub_auditory_roughness_Fig6_10, table2array(T(2601:2604,2)), 'b-o', 'DisplayName', 'Φ > 0', LineWidth=1.5)
hold on;
plot([pi/6 pi/3 pi/2], flipud(table2array(T(2605:2607,2))), 'b--o', 'DisplayName', 'Φ < 0', LineWidth=1.5)
legend('show');
title('auditory_roughness Fig6.10 8000Hz');
xlabel('|Φ|');
ylabel('Subjective Roughness [asper]');