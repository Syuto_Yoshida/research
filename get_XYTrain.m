function [XTrain, YTrain, n] = get_XYTrain(XTrain, YTrain, numFiles, segmentNum, n, audioDir, sampleNum, T, sr)

shift = (sr - sampleNum) / (segmentNum - 1);

for i=1:numFiles
    x = audioread(append(audioDir, int2str(i), '.wav'));
    startSample=1;
    endSample=sampleNum;
    for j=1:segmentNum
        XTrain{n} = x(startSample:endSample).';
        YTrain(n) = table2array(T(i,2));
        startSample=startSample + shift;
        endSample=startSample+sampleNum-1;
        n=n+1;
    end
end

end
