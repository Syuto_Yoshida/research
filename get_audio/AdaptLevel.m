function outSignaldB = AdaptLevel(inSignal, indB)

% indB = db - maxdB (maxdB is 100)
theRMS = rms(inSignal);
theFactor = 10^(indB/20 - log10(theRMS));
outSignaldB = inSignal*theFactor;

end