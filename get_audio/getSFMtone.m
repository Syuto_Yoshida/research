% This script derives an formula that defines the sinusoidally frequency-modulated tones
% based on the input values. This formula corresponds to VENCOVSKY's paper
% Roughness Prediction Based on a Model of Cochlear Hydrodynamics. figures 11, 12.
%
% SYNOPSIS: s=getSFMtone(fc, fm, df, time, sr, dB, plots)
%
% INPUT fc: carrier frequency (Hz)
%       fm: modulation frequency (Hz)
%       df: frequency deviation (Hz)
%       time: file size (ms)
%       sr: sample rate
%       dB: scale value (SPL)
%       plots: 0 or 1 
%
% OUTPUT s: the signal
%
% REMARKS 
%
% AUTHOR    : Syuto Yoshida
% $DATE     : 2022-11-4
% $Revision : 1.00 $
% DEVELOPED : Version of Matlab

function s = getSFMtone(fc, fm, df, time, sr, dB, plots)

if nargin<7
    plots=0;
end

t = linspace(0,time/1000,sr*time/1000);
 
s = sin(2*pi*fc*t-(df/fm)*cos(2*pi*fm*t));

if plots == 1
    plot(s);
end

if(dB < 100)
    % Set the scale value(SPL) to desired one
    s = AdaptLevel(s,dB-100); 
end

end