% This script derives an formula that defines the frequency modulate tone
% based on the input values. This formula corresponds to Daniel and Weber's paper
% Psychoacoustical Roughness: Implementation of an Optimized Model. figures 9 to 11.
%
% SYNOPSIS: s=getAudioSignal_DanielAndWeber1997_Fig9_11(fc, fm, time, sr, dB, fd, plots)
%
% INPUT fc: carrier frequency (Hz)
%       fm: modulation frequency (Hz)
%       time: file size (ms)
%       sr: sample rate
%       dB: scale value (SPL)
%       fd: delta frequency (Hz)
%       plots: 0 or 1 
%
% OUTPUT s: the signal
%
% REMARKS 
%
% SEE ALSO  ...
%
% AUTHOR    : Syuto Yoshida
% $DATE     : 2022-05-31
% $Revision : 1.00 $
% DEVELOPED : Version of Matlab

function s = getFMtone(fc, fm, time, sr, dB, fd, plots)

if nargin<7
    plots=0;
end

t = linspace(0,time/1000,sr*time/1000);
 
s = sin(2*pi*fc*t-((fd/fm)*cos(2*pi*fm*t)));

if plots == 1
    plot(s);
end

if(dB < 100)
    % Set the scale value(SPL) to desired one
    s = AdaptLevel(s,dB-100); 
end

end