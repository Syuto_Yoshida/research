% This script derives an formula that defines the sawtooth wave
% based on the input values. This formula corresponds to Pressnitzer and McAdams's paper
% Two phase effects in roughness perception. figures 8.
%
% SYNOPSIS: s=getAudioSignal_PressnitzerAndMcAdams1999_Fig8(fc, fm, time, sr, dB, m, num)
%
% INPUT fc: carrier frequency (Hz)
%       fm: modulation frequency (Hz)
%       time: file size (ms)
%       sr: sample rate
%       dB: scale value (SPL)
%       m: modulation depth
%       plots: 0 or 1 
%
% OUTPUT s: the signal
%
% REMARKS 
%
% AUTHOR    : Syuto Yoshida
% $DATE     : 2022-05-31
% $Revision : 1.00 $
% DEVELOPED : Version of Matlab

function s = getAudioSignal_PressnitzerAndMcAdams1999_Fig8(fc, fm, time, sr, dB, m, reverse)

ERB = 6.23 * (fc/1000)^2 + 93.39 * fc/1000 + 28.52;

N = 0;
while (N+1)*fm <= ERB/2
    N = N + 1;       
end

t = linspace(0,time/1000,sr*time/1000);

E = zeros(1, 48000);
for i=1:48000
    for n=1:N
        E(i) = E(i) + (1/n)*cos(2*pi*n*fm*t(i)-pi/2);
    end
end

% syms n 
% E = symsum((1/n)*cos(2*pi*n*fm*t-pi/2), n, 1, N);

Emax = max(E);

s = size(sr*time/1000);
for i=1:sr*time/1000
    s(i) = (1+m*E(i)/Emax)*cos(2*pi*fc*t(i)-pi/2);
end

if(dB < 100)
    % Set the scale value(SPL) to desired one
    s = AdaptLevel(s,dB-100); 
end

if reverse
    s = flip(s);
end