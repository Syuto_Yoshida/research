% This script derives an formula that defines the unmodulated bandpass noise
% based on the input values. This formula corresponds to Daniel and Weber's paper
% Psychoacoustical Roughness: Implementation of an Optimized Model. figures 8.
%
% SYNOPSIS: s=getUnmodulatedBandpassNoise(fc, BW, time, sr, dB, plots)
%
% INPUT fc: center frequency (Hz)
%       BW: band width
%       time: file size (ms)
%       sr: sample rate
%       dB: scale value (SPL)
%       plots: 0 or 1 
%
% OUTPUT s: the signal
%
% REMARKS 
%
% AUTHOR    : Syuto Yoshida
% $DATE     : 2022-05-31
% $Revision : 1.00 $
% DEVELOPED : Version of Matlab
function [s, fc, BW, filter_order, slope] = getUnmodulatedBandpassNoise(fc, BW, time, sr, dB, plots)

if nargin<7
    plots=0;
end

% s = 2*rand(1, sr*time/1000)-1;

s = wgn(1, sr*time/1000);
s = s / max(abs(s));

pbf1 = fc - BW/2; 
pbf2 = fc + BW/2;

filter_order = 16;

if fc == 125
    switch BW
        case 10
            filter_order = 16; % 244
        case 20
            filter_order = 18; % 227
        case 30
            filter_order = 22; % 246
        case 40
            filter_order = 24; % 245
        case 50
            filter_order = 26; % 247
        case 60
            filter_order = 26; % 232
        case 70
            filter_order = 28; % 237
        case 80
            filter_order = 30; % 243
        case 90
            filter_order = 30; % 232
        case 100
            filter_order = 32; % 239
        case 110
            filter_order = 32; % 231
        case 120
            filter_order = 34; % 238
        case 130
            filter_order = 34; % 231
        case 140
            filter_order = 36; % 239
        case 150
            filter_order = 38; % 247
        case 160
            filter_order = 38; % 241
        case 170
            filter_order = 38; % 236
        case 180
            filter_order = 38; % 231
        case 190
            filter_order = 40; % 239
        case 200
            filter_order = 40;
        case 210
            filter_order = 42;
        case 220
            filter_order = 42;
        case 230
            filter_order = 42;
        case 240
            filter_order = 44;
    end
end

if fc == 250
    if BW <= 10
        filter_order = 14;
    elseif BW <= 20 
        filter_order = 16;
    elseif BW <= 30
        filter_order = 18;
    elseif BW <= 50
        filter_order = 20;
    elseif BW <= 70
        filter_order = 22;
    elseif BW <= 90
        filter_order = 24;
    elseif BW <= 120
        filter_order = 26;
    elseif BW <= 140
        filter_order = 28;
    elseif BW <= 180
        filter_order = 30;
    elseif BW <= 210
        filter_order = 32;
    elseif BW <= 250
        filter_order = 34;
    elseif BW <= 290
        filter_order = 36;
    elseif BW <= 340
        filter_order = 38;
    elseif BW <= 400
        filter_order = 40;
    elseif BW <= 460
        filter_order = 42;
    else 
        filter_order = 44;
    end
end

if fc == 500
    if BW <= 10 
        filter_order = 12;
    elseif BW <= 30
        filter_order = 14;
    elseif BW <= 50 
        filter_order = 16;
    elseif BW <= 80
        filter_order = 18;
    elseif BW <= 100
        filter_order = 20;
    elseif BW <= 140 
        filter_order = 22;
    elseif BW <= 190
        filter_order = 24;
    elseif BW <= 240
        filter_order = 26;
    elseif BW <= 290
        filter_order = 28;
    elseif BW <= 360
        filter_order = 30;
    elseif BW <= 430
        filter_order = 32;
    elseif BW <= 510
        filter_order = 34;
    elseif BW <= 600
        filter_order = 36;
    elseif BW <= 700
        filter_order = 38;
    elseif BW <= 810
        filter_order = 40;
    elseif BW <= 930
        filter_order = 42;
    else 
        filter_order = 44;
    end
end

if fc == 1000
    if BW <= 10
        filter_order = 10;
    elseif BW <= 30 
        filter_order = 12;
    elseif BW <= 60
        filter_order = 14;
    elseif BW <= 100 
        filter_order = 16;
    elseif BW <= 160
        filter_order = 18;
    elseif BW <= 220
        filter_order = 20;
    elseif BW <= 300 
        filter_order = 22;
    elseif BW <= 390
        filter_order = 24;
    elseif BW <= 490
        filter_order = 26;
    elseif BW <= 600
        filter_order = 28;
    elseif BW <= 720
        filter_order = 30;
    elseif BW <= 880
        filter_order = 32;
    elseif BW <= 1030
        filter_order = 34;
    elseif BW <= 1220
        filter_order = 36;
    elseif BW <= 1430
        filter_order = 38;
    elseif BW <= 1670
        filter_order = 40;
    elseif BW <= 1950
        filter_order = 42;
    else 
        filter_order = 44;
    end
end

if fc == 2000
    if BW <= 30
        filter_order = 10;
    elseif BW <= 70
        filter_order = 12;
    elseif BW <= 130
        filter_order = 14;
    elseif BW <= 220
        filter_order = 16;
    elseif BW <= 320 
        filter_order = 18;
    elseif BW <= 460
        filter_order = 20;
    elseif BW <= 610
        filter_order = 22;
    elseif BW <= 800
        filter_order = 24;
    elseif BW <= 1010
        filter_order = 26;
    elseif BW <= 1250
        filter_order = 28;
    elseif BW <= 1530
        filter_order = 30;
    elseif BW <= 1850
        filter_order = 32;
    elseif BW <= 2220
        filter_order = 34;
    elseif BW <= 2670
        filter_order = 36;
    elseif BW <= 3190
        filter_order = 38;
    elseif BW <= 3830
        filter_order = 40;
    else 
        filter_order = 42;
    end
end

if fc == 4000
    if BW <= 20
        filter_order = 8;
    elseif BW <= 60
        filter_order = 10;
    elseif BW <= 150
        filter_order = 12;
    elseif BW <= 290
        filter_order = 14;
    elseif BW <= 470 
        filter_order = 16;
    elseif BW <= 700
        filter_order = 18;
    elseif BW <= 1000
        filter_order = 20;
    elseif BW <= 1370
        filter_order = 22;
    elseif BW <= 1810
        filter_order = 24;
    elseif BW <= 2380
        filter_order = 26;
    elseif BW <= 3070
        filter_order = 28;
    elseif BW <= 4090
        filter_order = 30;
    elseif BW <= 6090
        filter_order = 32;
    else 
        filter_order = 34;
    end
end

if pbf1 == 0
    switch fc
        case 125
            filter_order = 22; % 240
        case 250
            filter_order = 22; % 240
        case 500
            filter_order = 22; % 241
        case 1000
            filter_order = 22; % 244
        case 2000
            filter_order = 21; % 244
        case 4000
            filter_order = 16; % 233
    end
end

% pass band frequency is 0 in bandpassiir, an error occurs
% so use a low pass filter instead.
if pbf1 == 0
    bpFilt = designfilt('lowpassiir','FilterOrder',filter_order, ...
         'PassbandFrequency',pbf2, ...
         'SampleRate',48000);
else
    bpFilt = designfilt('bandpassiir','FilterOrder',filter_order, ...
         'PassbandFrequency1',pbf1,'PassbandFrequency2',pbf2, ...
         'SampleRate',48000);
end

[h,w] = freqz(bpFilt,sr);
%x = w*24000/pi;
y = db(h);
%plot(w*24000/pi,db(h));
%y(pbf2*4+1)
slope = round(y(pbf2*4+1));
%sprintf('%d: %f',BW,slope);
%fvtool(bpFilt);

s = filter(bpFilt, s);

%figure
%y = fft(s);
%f = 0:48000-1;
%power = abs(y).^2/48000;  

%plot(f,power)
%xlabel('Frequency')
%ylabel('Power')

s = s/max(abs(s));

AdaptLevel(s, dB-100);

end