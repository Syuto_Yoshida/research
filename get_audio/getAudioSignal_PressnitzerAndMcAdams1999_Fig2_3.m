% This script derives an formula that defines the amplitude modulate tone
% based on the input values. This formula corresponds to Pressnitzer and McAdams's paper
% Two phase effects in roughness perception. figures 2 and 3.
%
% SYNOPSIS: s=getAudioSignal_DanielAndWeber1997_Fig9_11(fc, fm, time, sr, dB, plots)
%
% INPUT fc: carrier frequency (Hz)
%       fm: modulation frequency (Hz)
%       time: file size (ms)
%       sr: sample rate
%       dB: scale value (SPL)
%       plots: 0 or 1 
%
% OUTPUT s: the signal
%
% REMARKS 
%
% AUTHOR    : Syuto Yoshida
% $DATE     : 2022-05-31
% $Revision : 1.00 $
% DEVELOPED : Version of Matlab

function s = getAudioSignal_PressnitzerAndMcAdams1999_Fig2_3(fc, fm, time, sr, dB, phase)

t = linspace(0,time/1000,sr*time/1000);
 
s = 0.5*cos(2*pi*(fc-fm)*t)+cos(2*pi*fc*t+phase)+0.5*cos(2*pi*(fc+fm)*t);

%plot(s) %optional plot==1 or 0

if(dB < 100)
    % Set the scale value(SPL) to desired one
    s = AdaptLevel(s,dB-100); 
end

end