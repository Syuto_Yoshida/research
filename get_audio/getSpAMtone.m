%
% SYNOPSIS: s=getSpAMtone(fc, fm, time, sr, dB, phase, plots)
%
% INPUT fc: carrier frequency (Hz)
%       fm: modulation frequency (Hz)
%       time: file size (ms)
%       sr: sample rate
%       dB: scale value (SPL)
%       phase: phase
%       plots: 0 or 1 
%
% OUTPUT s: the signal
%
% REMARKS 
%
% AUTHOR    : Syuto Yoshida
% $DATE     : 2022-11-2
% $Revision : 1.00 $
% DEVELOPED : Version of Matlab

function s = getSpAMtone(fc, fm, time, sr, dB, phase, plots)

if nargin<7
    plots=0;
end

t = linspace(0,time/1000,sr*time/1000);

s = (cos(2*pi*(fc-fm)*t))/2 + cos(2*pi*fc*t+phase) + (cos(2*pi*(fc+fm)*t))/2;


% Set the scale value(SPL) to desired one
s = AdaptLevel(s,dB-100); 

if plots == 1
    plot(s);
end

end