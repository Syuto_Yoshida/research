% This script derives an formula that defines the amplitude modulate tone
% based on the input values. This formula corresponds to Daniel and Weber's paper
% Psychoacoustical Roughness: Implementation of an Optimized Model. figures 4.
%
% SYNOPSIS: s=getSAMtone_Loudness6sone(fc, fm, time, sr, m, plots)
%
% INPUT fc: carrier frequency (Hz)
%       fm: modulation frequency (Hz)
%       time: file size (ms)
%       sr: sample rate
%       m: modulation depth
%       plots: 0 or 1 
%
% OUTPUT s: the signal
%
% REMARKS 
%
% AUTHOR    : Syuto Yoshida
% $DATE     : 2022-05-31
% $Revision : 1.00 $
% DEVELOPED : Version of Matlab

function s = getSAMtone_Loudness6sone(fc, fm, time, sr, m, plots)

if nargin<7
    plots=0;
end

t = linspace(0,time/1000,sr*time/1000);

s = (1+m*cos(2*pi*fm*t)).*cos(2*pi*fc*t);  


% Set the scale value(SPL) to desired one
for dB=50:0.01:200
    s=AdaptLevel(s, dB-100);
    loudness=acousticLoudness(s.', sr);
    if(loudness >= 6)
        break
    end
end

if plots == 1
    plot(s);
end

end