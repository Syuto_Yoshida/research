% This script derives an formula that defines the two tone stimuli composed of 
% six harmonic complex tones based on the input values. This formula corresponds to VENCOVSKY's paper
% Roughness Prediction Based on a Model of Cochlear Hydrodynamics. figures 14.
%
% SYNOPSIS: s=getMusicalScaleTone(fc, N, time, sr, dB, plots)
%
% INPUT fc: carrier frequency (Hz)
%       N: number that determines a particular scale in 12 tone scale
%       time: file size (ms)
%       sr: sample rate
%       dB: scale value (SPL)
%       plots: 0 or 1 
%
% OUTPUT s: the signal
%
% REMARKS 
%
% AUTHOR    : Syuto Yoshida
% $DATE     : 2022-11-4
% $Revision : 1.00 $
% DEVELOPED : Version of Matlab

function s = getMusicalScaleTone(fc, N, time, sr, dB, plots)

if nargin<6
    plots=0;
end

t = linspace(0,time/1000,sr*time/1000);
 
s=0;

% stimuli composed by two tones (C4 + scale specified by N)
% each tones has six harmonics
for n=1:6
  s = s + (sin(2*pi*fc*n*t)+sin(2*pi*fc*n*t))/n + (sin(2*pi*fc*n*t)+sin(2*pi*fc*(2^(N/120))*n*t))/n;
end

if plots == 1
    plot(s);
end

if(dB < 100)
    % Set the scale value(SPL) to desired one
    s = AdaptLevel(s,dB-100); 
end

end