% This script uses bilstmLayer to train the model. 
% This model takes audio segment as input and outputs the roughness.
% 
% segmentNum : Number of segments obtained by splitting from a single 1000 ms audio
% audioLen : length of audio segment for trainig
% sr : sample rate
% 
% AUTHOR    : Syuto Yoshida
% $DATE     : 2023-10-9
% $Revision : 1.00 $
% DEVELOPED : R2022b

segmentNum = 9;
audioLen = 200;
sr = 48000; 

[XTrain, YTrain, XTest, YTest, XValidation, YValidation] = get_training_data(segmentNum, audioLen, sr);

mean = mean(YTrain)
max = max(YTrain)
min = min(YTrain)

YTrain_r = round(YTrain, 5)

numFeatures = 1;

numHiddenUnits1 = 128;
numHiddenUnits2 = 64;

layers = [
        sequenceInputLayer(numFeature,'Name','input')
        bilstmLayer(64,'OutputMode','sequence','Name','lstm1')
        layerNormalizationLayer('Name','bn1')
        dropoutLayer(0.2)
        bilstmLayer(32,'OutputMode','sequence','Name','lstm2')
        layerNormalizationLayer('Name','bn2')
        dropoutLayer(0.2)
        bilstmLayer(16,'OutputMode','last','Name','lstm3')
        layerNormalizationLayer('Name','bn3')
        dropoutLayer(0.2)$
        fullyConnectedLayer(1,'Name','fc')
        regressionLayer('Name','reg')
        ];


net = neuralNetwork();
numHiddenUnits = 128; % BiLSTMの隠れユニット数

% BiLSTMレイヤーの追加
net = addLayers(net, bilstmLayer(numHiddenUnits, 'OutputMode', 'sequence'), 'input');

% 出力層の追加（回帰タスクの場合、線形出力を使用します）
net = addLayers(net, regressionLayer(), 'bilstm');

% ネットワークを接続
net = connectLayers(net, 'input', 'bilstm');

% ネットワークの表示（オプション）
view(net);

options = trainingOptions('adam', ...
    ExecutionEnvironment = "gpu", ...
    InitialLearnRate = 0.001, ...
    LearnRateSchedule = "piecewise", ...
    LearnRateDropPeriod=5, ...
    LearnRateDropFactor=0.5, ...
    MaxEpochs = 40, ...
    MiniBatchSize = 128, ...   
    GradientThresholdMethod = "l2norm", ...    
    GradientThreshold = 5, ...
    L2Regularization = 0.0001, ...
    Shuffle = 'once', ...
    Verbose = 1, ...
    ValidationData = {XValidation,YValidation}, ...
    ValidationFrequency=50, ...  
    Plots = "none");

% net = trainNetwork(XTrain,YTrain,layers,options);
net = trainNetwork(XTrain,YTrain,net,options);