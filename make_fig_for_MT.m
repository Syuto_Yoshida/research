%% Test the roughness of dependence of modulation rate in SAM tone 
% Daniel and Weber Fig.3 
% fc=125(Hz), m=1, 60dB

N = 101; 
YPred_DanielAndWeber_Fig3_125 = zeros(1, N);
acoustic_roughness_DanielAndWeber_Fig3_125 = zeros(1, N);


for n=1:N
    fileName = append('audio/audio_SAM_modulation_frequency/audio', int2str(n), '.wav');
    [x, Fs] = audioread(fileName);
    YPred_DanielAndWeber_Fig3_125(1, n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    roughness = median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]);
    acoustic_roughness_DanielAndWeber_Fig3_125(1, n) = mean(roughness);
end

XFmod_DanielAndWeber_Fig3_125 = 0:N-1; % x-axis value

% Daniel and Weber Fig.3 
% fc=500(Hz), m=1, 60dB

N = 201;
YPred_DanielAndWeber_Fig3_500 = zeros(1, N);
acoustic_roughness_DanielAndWeber_Fig3_500 = zeros(1, N);

for n=1:N
    fileName = append('audio/audio_SAM_modulation_frequency/audio', int2str(n+101), '.wav');
    [x, Fs] = audioread(fileName);
    YPred_DanielAndWeber_Fig3_500(1, n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    roughness = median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]);
    acoustic_roughness_DanielAndWeber_Fig3_500(1, n) = mean(roughness);
end

XFmod_DanielAndWeber_Fig3_500 = 0:N-1; % x-axis value

% Daniel and Weber Fig.3 
% fc=8000(Hz), m=1, 60dB

N = 301;
YPred_DanielAndWeber_Fig3_8000 = zeros(1, N);
acoustic_roughness_DanielAndWeber_Fig3_8000 = zeros(1, N);

for n=1:N
    fileName = append('audio/audio_SAM_modulation_frequency/audio', int2str(n+302), '.wav');
    [x, Fs] = audioread(fileName);
    YPred_DanielAndWeber_Fig3_8000(n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    roughness = median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]);
    acoustic_roughness_DanielAndWeber_Fig3_8000(n) = mean(roughness);
end

XFmod_DanielAndWeber_Fig3_8000 = 0:N-1; % x-axis value

% Daniel and Weber Fig.3 
% fc=2000(Hz), m=1, 60dB

N=301;
YPred_DanielAndWeber_Fig3_2000 = zeros(1, N);
acoustic_roughness_DanielAndWeber_Fig3_2000 = zeros(1, N);

for n=1:N
    fileName = append('audio/audio_SAM_modulation_frequency/audio', int2str(n+603), '.wav');
    [x, Fs] = audioread(fileName);
    YPred_DanielAndWeber_Fig3_2000(n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    roughness = median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]);
    acoustic_roughness_DanielAndWeber_Fig3_2000(n) = mean(roughness);
end

XFmod_DanielAndWeber_Fig3_2000 = 0:N-1; % x-axis value

% Daniel and Weber Fig.3 
% fc=1000(Hz), m=1, 60dB

N=301;
YPred_DanielAndWeber_Fig3_1000 = zeros(1, N);
acoustic_roughness_DanielAndWeber_Fig3_1000 = zeros(1, N);

for n=1:N
    fileName = append('audio/audio_SAM_modulation_frequency/audio', int2str(n+904), '.wav');
    [x, Fs] = audioread(fileName);
    YPred_DanielAndWeber_Fig3_1000(n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    roughness = median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]);
    acoustic_roughness_DanielAndWeber_Fig3_1000(n) = mean(roughness);
end

XFmod_DanielAndWeber_Fig3_1000 = 0:N-1; % x-axis value

% Daniel and Weber Fig.3 
% fc=4000(Hz), m=1, 60dB

N=301; 
YPred_DanielAndWeber_Fig3_4000 = zeros(1, N);
acoustic_roughness_DanielAndWeber_Fig3_4000 = zeros(1, N);

for n=1:N
    fileName = append('audio/audio_SAM_modulation_frequency/audio', int2str(n+1205), '.wav');
    [x, Fs] = audioread(fileName);
    YPred_DanielAndWeber_Fig3_4000(n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    roughness = median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]);
    acoustic_roughness_DanielAndWeber_Fig3_4000(n) = mean(roughness);
end

XFmod_DanielAndWeber_Fig3_4000 = 0:N-1; % x-axis value

% Daniel and Weber Fig.3 
% fc=250(Hz), m=1, 60dB

N=201;
YPred_DanielAndWeber_Fig3_250 = zeros(1, N);
acoustic_roughness_DanielAndWeber_Fig3_250 = zeros(1, N);

for n=1:N
    fileName = append('audio/audio_SAM_modulation_frequency/audio', int2str(n+1506), '.wav');
    [x, Fs] = audioread(fileName);
    YPred_DanielAndWeber_Fig3_250(n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    roughness = median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]);
    acoustic_roughness_DanielAndWeber_Fig3_250(n) = mean(roughness);
end

XFmod_DanielAndWeber_Fig3_250 = 0:N-1; % x-axis value

% Subjective Value

T = readtable('roughness_test_sub_data.xlsx');

%　Data preparation 
y_sub_DanielAndWeber_Fig3_125 = table2array(T(1:9,2));
x_sub_DanielAndWeber_Fig3_125 = linspace(20, 100, 9);

y_sub_DanielAndWeber_Fig3_500 = table2array(T(10:24,2));
x_sub_DanielAndWeber_Fig3_500 = linspace(20, 160, 15);

y_sub_DanielAndWeber_Fig3_8000 = table2array(T(25:39,2));
x_sub_DanielAndWeber_Fig3_8000 = linspace(20, 160, 15);

y_sub_DanielAndWeber_Fig3_2000 = table2array(T(40:54,2));
x_sub_DanielAndWeber_Fig3_2000 = linspace(20, 160, 15);

y_sub_DanielAndWeber_Fig3_1000 = table2array(T(55:69,2));
x_sub_DanielAndWeber_Fig3_1000 = linspace(20, 160, 15);

y_sub_DanielAndWeber_Fig3_4000 = table2array(T(70:84,2));
x_sub_DanielAndWeber_Fig3_4000 = linspace(20, 160, 15);

y_sub_DanielAndWeber_Fig3_250 = table2array(T(85:96,2));
x_sub_DanielAndWeber_Fig3_250 = linspace(20, 130, 12);

figure('Position',[0 0 900 500]);

t = tiledlayout(2,2,'TileSpacing','Compact','Padding','Compact');

t1 = nexttile;
plot(XFmod_DanielAndWeber_Fig3_125, YPred_DanielAndWeber_Fig3_125, 'k-', DisplayName='ML model',LineWidth=1.5)
hold on;
plot(XFmod_DanielAndWeber_Fig3_125, acoustic_roughness_DanielAndWeber_Fig3_125, 'b--+', DisplayName='standardized model',LineWidth=1)
plot(x_sub_DanielAndWeber_Fig3_125, y_sub_DanielAndWeber_Fig3_125, 'b--o', DisplayName='subjective',LineWidth=1);
title(t1,'$f_{c}$=$125$\,Hz','Interpreter','latex','FontSize',16);
window = legend('show','Location','northeast');
set(window, 'Interpreter','latex')
% xlabel(t, '$f_{mod}$\,[Hz]', 'Interpreter','latex','FontSize',16);
% ylabel(t, '$R$\,[asper]', 'Interpreter','latex','FontSize',16);
% xlim([0 300]);
ylim([0 0.6]);
set(gca,'FontSize',18,'FontName','Times New Roman');

% t2 = nexttile;
% plot(XFmod_DanielAndWeber_Fig3_250, YPred_DanielAndWeber_Fig3_250, 'k-', DisplayName='ML model', LineWidth=1.5)
% hold on;
% plot(XFmod_DanielAndWeber_Fig3_250, acoustic_roughness_DanielAndWeber_Fig3_250, 'b--+', DisplayName='standardized model',LineWidth=1)
% plot(x_sub_DanielAndWeber_Fig3_250, y_sub_DanielAndWeber_Fig3_250, 'b--o', DisplayName='subjective',LineWidth=1);
% title(t2,'$f_{c}$=$250$\,Hz','Interpreter','latex','FontSize',16);
% % xlabel(t, '$f_{mod}$\,[Hz]', 'Interpreter','latex','FontSize',16);
% % ylabel(t, '$R$\,[asper]', 'Interpreter','latex','FontSize',16);
% % xlim([0 300]);
% % ylim([0 1.2]);
% set(gca,'FontSize',16,'FontName','Times New Roman');

t2 = nexttile;
plot(XFmod_DanielAndWeber_Fig3_500, YPred_DanielAndWeber_Fig3_500, 'k-', DisplayName='ML model', LineWidth=1.5)
hold on;
plot(XFmod_DanielAndWeber_Fig3_500, acoustic_roughness_DanielAndWeber_Fig3_500, 'b--+', DisplayName='standardized model', LineWidth=1)
plot(x_sub_DanielAndWeber_Fig3_500, y_sub_DanielAndWeber_Fig3_500, 'b--o', DisplayName='subjective',LineWidth=1);
title(t2,'$f_{c}$=$500$\,Hz','Interpreter','latex','FontSize',16);
% xlabel(t, '$f_{mod}$\,[Hz]', 'Interpreter','latex','FontSize',16);
% ylabel(t, '$R$\,[asper]', 'Interpreter','latex','FontSize',16);
% xlim([0 300]);
ylim([0 1]);
set(gca,'FontSize',18,'FontName','Times New Roman');

% t4 = nexttile;
% plot(XFmod_DanielAndWeber_Fig3_1000, YPred_DanielAndWeber_Fig3_1000, 'k-', DisplayName='ML model', LineWidth=1.5)
% hold on;
% plot(XFmod_DanielAndWeber_Fig3_1000, acoustic_roughness_DanielAndWeber_Fig3_1000, 'b--+', DisplayName='standardized model', LineWidth=1)
% plot(x_sub_DanielAndWeber_Fig3_1000, y_sub_DanielAndWeber_Fig3_1000, 'b--o', DisplayName='subjective',LineWidth=1);
% title(t4,'$f_{c}$=$1000$\,Hz','Interpreter','latex','FontSize',16);
% % xlabel(t, '$f_{mod}$\,[Hz]', 'Interpreter','latex','FontSize',16);
% % ylabel(t, '$R$\,[asper]', 'Interpreter','latex','FontSize',16);
% % xlim([0 300]);
% % ylim([0 1.2]);
% set(gca,'FontSize',16,'FontName','Times New Roman');

t3 = nexttile;
plot(XFmod_DanielAndWeber_Fig3_2000, YPred_DanielAndWeber_Fig3_2000, 'k-', DisplayName='ML model', LineWidth=1.5)
hold on;
plot(XFmod_DanielAndWeber_Fig3_2000, acoustic_roughness_DanielAndWeber_Fig3_2000, 'b--+', DisplayName='ISO 532-1', LineWidth=1)
plot(x_sub_DanielAndWeber_Fig3_2000, y_sub_DanielAndWeber_Fig3_2000, 'b--o', DisplayName='subjective',LineWidth=1);
title(t3,'$f_{c}$=$2000$\,Hz','Interpreter','latex','FontSize',16);
% xlabel(t, '$f_{mod}$\,[Hz]', 'Interpreter','latex','FontSize',16);
% ylabel(t, '$R$\,[asper]', 'Interpreter','latex','FontSize',16);
% xlim([0 300]);
% ylim([0 1.2]);
set(gca,'FontSize',18,'FontName','Times New Roman');

% t6 = nexttile;
% plot(XFmod_DanielAndWeber_Fig3_4000, YPred_DanielAndWeber_Fig3_4000, 'k-', DisplayName='ML model', LineWidth=1.5)
% hold on;
% plot(XFmod_DanielAndWeber_Fig3_4000, acoustic_roughness_DanielAndWeber_Fig3_4000, 'b--+', DisplayName='ISO 532-1',LineWidth=1)
% plot(x_sub_DanielAndWeber_Fig3_4000, y_sub_DanielAndWeber_Fig3_4000, 'b--o', DisplayName='subjective',LineWidth=1);
% title(t6,'$f_{c}$=$4000$\,Hz','Interpreter','latex','FontSize',16);
% % xlabel(t, '$f_{mod}$\,[Hz]', 'Interpreter','latex','FontSize',16);
% % ylabel(t, '$R$\,[asper]', 'Interpreter','latex','FontSize',16);
% % xlim([0 300]);
% ylim([0 1]);
% set(gca,'FontSize',16,'FontName','Times New Roman');

t4 = nexttile;
plot(XFmod_DanielAndWeber_Fig3_8000, YPred_DanielAndWeber_Fig3_8000, 'k-', DisplayName='ML model', LineWidth=1.5)
hold on;
plot(XFmod_DanielAndWeber_Fig3_8000, acoustic_roughness_DanielAndWeber_Fig3_8000, 'b--+', DisplayName='ISO 532-1', LineWidth=1)
plot(x_sub_DanielAndWeber_Fig3_8000, y_sub_DanielAndWeber_Fig3_8000, 'b--o', DisplayName='subjective',LineWidth=1);
title(t4,'$f_{c}$=$8000$\,Hz','Interpreter','latex','FontSize',16);
% xlabel(t, '$f_{mod}$\,[Hz]', 'Interpreter','latex','FontSize',16);
% ylabel(t, '$R$\,[asper]', 'Interpreter','latex','FontSize',16);
% xlim([0 300]);
ylim([0 0.5]);
set(gca,'FontSize',18,'FontName','Times New Roman');

xlabel(t, '$f_{mod}$\,[Hz]', 'Interpreter','latex','FontSize',18);
ylabel(t, '$R$\,[asper]', 'Interpreter','latex','FontSize',18);

%% Test roughness in dependence on phase in pAM tone
% Pressnitzer&McAdams1999 figure 2, 3

N = 4;

% fc=125, fmod=30, 60dB

XPhase = [0 pi/6 pi/3 pi/2];
YPredPhase_125_p = zeros(1, N);
YPredPhase_125_n = zeros(1, N);
acoustic_roughness_Phase_125_p = zeros(1, N);
acoustic_roughness_Phase_125_n = zeros(1, N);

for n=1:4
    fileName_p = append('audio/audio_pSAM_phase/audio', int2str(n), '.wav');
    if n == 1
        fileName_n = fileName_p;
    else
        fileName_n = append('audio/audio_pSAM_phase/audio', int2str(9-n), '.wav');
    end
    [x, Fs] = audioread(fileName_p);
    YPredPhase_125_p(n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    acoustic_roughness_Phase_125_p(n) = mean(median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]));
    [x, Fs] = audioread(fileName_n);
    YPredPhase_125_n(n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    acoustic_roughness_Phase_125_n(n) = mean(median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]));
end

% fc=250, fmod=40, 60dB

YPredPhase_250_p = zeros(1, N);
YPredPhase_250_n = zeros(1, N);
acoustic_roughness_Phase_250_p = zeros(1, N);
acoustic_roughness_Phase_250_n = zeros(1, N);

for n=1:4
    fileName_p = append('audio/audio_pSAM_phase/audio', int2str(n+7), '.wav');
    if n == 1
        fileName_n = fileName_p;
    else
        fileName_n = append('audio/audio_pSAM_phase/audio', int2str(16-n), '.wav');
    end
    [x, Fs] = audioread(fileName_p);
    YPredPhase_250_p(n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    acoustic_roughness_Phase_250_p(n) = mean(median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]));
    [x, Fs] = audioread(fileName_n);
    YPredPhase_250_n(n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    acoustic_roughness_Phase_250_n(n) = mean(median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]));
end

% fc=500, fmod=50, 60dB

YPredPhase_500_p = zeros(1, N);
YPredPhase_500_n = zeros(1, N);
acoustic_roughness_Phase_500_p = zeros(1, N);
acoustic_roughness_Phase_500_n = zeros(1, N);

for n=1:4
    fileName_p = append('audio/audio_pSAM_phase/audio', int2str(n+14), '.wav');
    if n == 1
        fileName_n = fileName_p;
    else
        fileName_n = append('audio/audio_pSAM_phase/audio', int2str(23-n), '.wav');
    end
    [x, Fs] = audioread(fileName_p);
    YPredPhase_500_p(n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    acoustic_roughness_Phase_500_p(n) = mean(median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]));
    [x, Fs] = audioread(fileName_n);
    YPredPhase_500_n(n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    acoustic_roughness_Phase_500_n(n) = mean(median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]));
end

% fc=1000, fmod=70, 60dB

YPredPhase_1000_p = zeros(1, N);
YPredPhase_1000_n = zeros(1, N);
acoustic_roughness_Phase_1000_p = zeros(1, N);
acoustic_roughness_Phase_1000_n = zeros(1, N);

for n=1:4
    fileName_p = append('audio/audio_pSAM_phase/audio', int2str(n+21), '.wav');
    if n == 1
        fileName_n = fileName_p;
    else
        fileName_n = append('audio/audio_pSAM_phase/audio', int2str(30-n), '.wav');
    end
    [x, Fs] = audioread(fileName_p);
    YPredPhase_1000_p(n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    acoustic_roughness_Phase_1000_p(n) = mean(median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]));
    [x, Fs] = audioread(fileName_n);
    YPredPhase_1000_n(n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    acoustic_roughness_Phase_1000_n(n) = mean(median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]));
end

% fc=2000, fmod=70, 60dB

YPredPhase_2000_p = zeros(1, N);
YPredPhase_2000_n = zeros(1, N);
acoustic_roughness_Phase_2000_p = zeros(1, N);
acoustic_roughness_Phase_2000_n = zeros(1, N);

for n=1:4
    fileName_p = append('audio/audio_pSAM_phase/audio', int2str(n+28), '.wav');
    if n == 1
        fileName_n = fileName_p;
    else
        fileName_n = append('audio/audio_pSAM_phase/audio', int2str(37-n), '.wav');
    end
    [x, Fs] = audioread(fileName_p);
    YPredPhase_2000_p(n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    acoustic_roughness_Phase_2000_p(n) = mean(median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]));
    [x, Fs] = audioread(fileName_n);
    YPredPhase_2000_n(n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    acoustic_roughness_Phase_2000_n(n) = mean(median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]));
end

% fc=4000, fmod=70, 60dB

YPredPhase_4000_p = zeros(1, N);
YPredPhase_4000_n = zeros(1, N);
acoustic_roughness_Phase_4000_p = zeros(1, N);
acoustic_roughness_Phase_4000_n = zeros(1, N);

for n=1:4
    fileName_p = append('audio/audio_pSAM_phase/audio', int2str(n+35), '.wav');
    if n == 1
        fileName_n = fileName_p;
    else
        fileName_n = append('audio/audio_pSAM_phase/audio', int2str(44-n), '.wav');
    end
    [x, Fs] = audioread(fileName_p);
    YPredPhase_4000_p(n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    acoustic_roughness_Phase_4000_p(n) = mean(median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]));
    [x, Fs] = audioread(fileName_n);
    YPredPhase_4000_n(n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    acoustic_roughness_Phase_4000_n(n) = mean(median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]));
end

% fc=8000, fmod=70, 60dB

YPredPhase_8000_p = zeros(1, N);
YPredPhase_8000_n = zeros(1, N);
acoustic_roughness_Phase_8000_p = zeros(1, N);
acoustic_roughness_Phase_8000_n = zeros(1, N);

for n=1:4
    fileName_p = append('audio/audio_pSAM_phase/audio', int2str(n+42), '.wav');
    if n == 1
        fileName_n = fileName_p;
    else
        fileName_n = append('audio/audio_pSAM_phase/audio', int2str(51-n), '.wav');
    end
    [x, Fs] = audioread(fileName_p);
    YPredPhase_8000_p(n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    acoustic_roughness_Phase_8000_p(n) = mean(median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]));
    [x, Fs] = audioread(fileName_n);
    YPredPhase_8000_n(n) = median([predict(net, x(1:9600).') predict(net, x(4801:14400).') predict(net, x(9601:19200).')]);
    acoustic_roughness_Phase_8000_n(n) = mean(median([acousticRoughness(x(1:9600),Fs) acousticRoughness(x(4801:14400),Fs) acousticRoughness(x(9601:19200),Fs)]));
end

T = readtable('roughness_test_sub_data.xlsx');
Y_PredPhase_125_p = table2array(T(228:231,2));
Y_PredPhase_125_n = table2array(T(232:235,2));
Y_PredPhase_250_p = table2array(T(236:239,2));
Y_PredPhase_250_n = table2array(T(240:243,2));
Y_PredPhase_500_p = table2array(T(244:247,2));
Y_PredPhase_500_n = table2array(T(248:251,2));
Y_PredPhase_1000_p = table2array(T(252:255,2));
Y_PredPhase_1000_n = table2array(T(256:259,2));
Y_PredPhase_2000_p = table2array(T(260:263,2));
Y_PredPhase_2000_n = table2array(T(264:267,2));
Y_PredPhase_4000_p = table2array(T(268:271,2));
Y_PredPhase_4000_n = table2array(T(272:275,2));
Y_PredPhase_8000_p = table2array(T(276:279,2));
Y_PredPhase_8000_n = table2array(T(280:283,2));

figure('Position',[0 0 900 500]);
t = tiledlayout(2,2,'TileSpacing','Compact','Padding','Compact');

t1 = nexttile;
plot(XPhase, YPredPhase_125_p, 'k-', DisplayName='ML model : $\Phi$\,$>$\,$0$', Linewidth=1);
hold on;
plot(XPhase, YPredPhase_125_n, 'k--', DisplayName='ML model : $\Phi$\,$<$\,$0$', Linewidth=1);
plot(XPhase, acoustic_roughness_Phase_125_p, 'b-+', DisplayName='standardized model : $\Phi$\,$>$\,$0$', Linewidth=1);
plot(XPhase, acoustic_roughness_Phase_125_n, 'b--+', DisplayName='standardized model : $\Phi$\,$<$\,$0$', Linewidth=1);
plot(XPhase, Y_PredPhase_125_p, 'b-o', DisplayName='calculated : $\Phi$\,$>$\,$0$', Linewidth=1);
plot(XPhase, Y_PredPhase_125_n, 'b--o', DisplayName='calculated : $\Phi$\,$<$\,$0$', Linewidth=1);
window  = legend('show','Location','northeast');
set(window, 'Interpreter','latex')
xticks([0 pi/6 pi/3 pi/2]);
xticklabels({'0', '$\pi$/6', '$\pi$/3', '$\pi$/2'});
xlim("padded");
ylim([0 1.2]);
title(t1,'$f_{c}$=$125$\,Hz, $f_{mod}$=$30$\,Hz','Interpreter','latex','FontSize',18);
set(gca,'FontSize',18,'FontName',  'Times New Roman');


% t2 = nexttile;
% plot(XPhase, YPredPhase_250_p, 'k-', 'DisplayName', 'ML model : Φ > 0', Linewidth=1);
% hold on;
% plot(XPhase, YPredPhase_250_n, 'k--', 'DisplayName', 'ML model : Φ < 0', Linewidth=1);
% plot(XPhase, acoustic_roughness_Phase_250_p, 'b-+', 'DisplayName', 'standardized model : Φ > 0', Linewidth=1);
% plot(XPhase, acoustic_roughness_Phase_250_n, 'b--+', 'DisplayName', 'standardized model : Φ < 0', Linewidth=1);
% plot(XPhase, Y_PredPhase_250_p, 'b-o', 'DisplayName', 'subjective : Φ > 0', Linewidth=1);
% plot(XPhase, Y_PredPhase_250_n, 'b--o', 'DisplayName', 'subjective : Φ < 0', Linewidth=1);
% xticks([0 pi/6 pi/3 pi/2]);
% xticklabels({'0', '$\pi$/6', '$\pi$/3', '$\pi$/2'});
% xlim("padded");
% ylim([0 1.2]);
% title(t2,'$f_{c}$=$250$\,Hz, $f_{mod}$=$40$\,Hz','Interpreter','latex','FontSize',16);
% set(gca,'FontSize',15,'FontName',  'Times New Roman');

t2 = nexttile;
plot(XPhase, YPredPhase_500_p, 'k-', 'DisplayName', 'ML model : Φ > 0', LineWidth=1);
hold on;
plot(XPhase, YPredPhase_500_n, 'k--', 'DisplayName', 'ML model : Φ < 0', LineWidth=1);
plot(XPhase, acoustic_roughness_Phase_500_p, 'b-+', 'DisplayName', 'standardized model : Φ > 0', Linewidth=1);
plot(XPhase, acoustic_roughness_Phase_500_n, 'b--+', 'DisplayName', 'standardized model : Φ < 0', Linewidth=1);
plot(XPhase, Y_PredPhase_500_p, 'b-o', 'DisplayName', 'subjective : Φ > 0', Linewidth=1);
plot(XPhase, Y_PredPhase_500_n, 'b--o', 'DisplayName', 'subjective : Φ < 0', Linewidth=1);
xticks([0 pi/6 pi/3 pi/2]);
xticklabels({'0', '$\pi$/6', '$\pi$/3', '$\pi$/2'});
xlim("padded");
ylim([0 1.2]);
title(t2,'$f_{c}$=$500$\,Hz, $f_{mod}$=$50$\,Hz','Interpreter','latex','FontSize',18);
set(gca,'FontSize',18,'FontName',  'Times New Roman');

% t4 = nexttile;
% plot(XPhase, YPredPhase_1000_p, 'k-', 'DisplayName', 'ML model : Φ > 0', Linewidth=1);
% hold on;
% plot(XPhase, YPredPhase_1000_n, 'k--', 'DisplayName', 'ML model : Φ < 0', Linewidth=1);
% plot(XPhase, acoustic_roughness_Phase_1000_p, 'b-+', 'DisplayName', 'standardized model: Φ > 0', Linewidth=1);
% plot(XPhase, acoustic_roughness_Phase_1000_n, 'b--+', 'DisplayName', 'standardized model: Φ < 0', Linewidth=1);
% plot(XPhase, Y_PredPhase_1000_p, 'b-o', 'DisplayName', 'subjective : Φ > 0', Linewidth=1);
% plot(XPhase, Y_PredPhase_1000_n, 'b--o', 'DisplayName', 'subjective : Φ < 0', Linewidth=1);
% xticks([0 pi/6 pi/3 pi/2]);
% xticklabels({'0', '$\pi$/6', '$\pi$/3', '$\pi$/2'});
% xlim("padded");
% ylim([0 1.2]);
% title(t4,'$f_{c}$=$1$\,kHz, $f_{mod}$=$70$\,Hz','Interpreter','latex','FontSize',16);
% set(gca,'FontSize',15,'FontName',  'Times New Roman');

t3 = nexttile;
plot(XPhase, YPredPhase_2000_p, 'k-', 'DisplayName', 'ML model : Φ > 0', LineWidth=1);
hold on;
plot(XPhase, YPredPhase_2000_n, 'k--', 'DisplayName', 'ML model : Φ < 0', LineWidth=1);
plot(XPhase, acoustic_roughness_Phase_2000_p, 'b-+', 'DisplayName', 'calculated : Φ > 0', Linewidth=1);
plot(XPhase, acoustic_roughness_Phase_2000_n, 'b--+', 'DisplayName', 'calculated : Φ < 0', Linewidth=1);
plot(XPhase, Y_PredPhase_2000_p, 'b-o', 'DisplayName', 'subjective : Φ > 0', Linewidth=1);
plot(XPhase, Y_PredPhase_2000_n, 'b--o', 'DisplayName', 'subjective : Φ < 0', Linewidth=1);
xticks([0 pi/6 pi/3 pi/2]);
xticklabels({'0', '$\pi$/6', '$\pi$/3', '$\pi$/2'});
xlim("padded");
ylim([0 1.2]);
title(t3,'$f_{c}$=$2$\,kHz, $f_{mod}$=$70$\,Hz','Interpreter','latex','FontSize',18);
set(gca,'FontSize',18,'FontName',  'Times New Roman');

% t6 = nexttile;
% plot(XPhase, YPredPhase_4000_p, 'k-', 'DisplayName', 'ML model : Φ > 0', Linewidth=1);
% hold on;
% plot(XPhase, YPredPhase_4000_n, 'k--', 'DisplayName', 'ML model : Φ < 0', Linewidth=1);
% plot(XPhase, acoustic_roughness_Phase_4000_p, 'b-+', 'DisplayName', 'standardized model: Φ > 0', Linewidth=1);
% plot(XPhase, acoustic_roughness_Phase_4000_n, 'b--+', 'DisplayName', 'standardized model: Φ < 0', Linewidth=1);
% plot(XPhase, Y_PredPhase_4000_p, 'b-o', 'DisplayName', 'subjective : Φ > 0', Linewidth=1);
% plot(XPhase, Y_PredPhase_4000_n, 'b--o', 'DisplayName', 'subjective : Φ < 0', Linewidth=1);
% xticks([0 pi/6 pi/3 pi/2]);
% xticklabels({'0', '$\pi$/6', '$\pi$/3', '$\pi$/2'});
% xlim("padded");
% ylim([0 1.2]);
% title(t6,'$f_{c}$=$4$\,kHz, $f_{mod}$=$70$\,Hz','Interpreter','latex','FontSize',15);
% set(gca,'FontSize',15,'FontName',  'Times New Roman');

set(groot, 'defaultAxesTickLabelInterpreter' , 'latex' ); 
t4 = nexttile;
plot(XPhase, YPredPhase_8000_p, 'k-', 'DisplayName', 'ML model: \Phi > 0', LineWidth=1);
hold on;
plot(XPhase, YPredPhase_8000_n, 'k--', 'DisplayName', 'ML model: \Phi < 0', LineWidth=1);
plot(XPhase, acoustic_roughness_Phase_8000_p, 'b-+', 'DisplayName', 'standardized model: \Phi > 0', Linewidth=1);
plot(XPhase, acoustic_roughness_Phase_8000_n, 'b--+', 'DisplayName', 'standardized model: \Phi < 0', Linewidth=1);
plot(XPhase, Y_PredPhase_8000_p, 'b-o', 'DisplayName', 'calculated : \Phi > 0', Linewidth=1);
plot(XPhase, Y_PredPhase_8000_n, 'b--o', 'DisplayName', 'calculated : \Phi < 0', Linewidth=1);
xticks([0 pi/6 pi/3 pi/2]);
xticklabels({'0', '$\pi$/6', '$\pi$/3', '$\pi$/2'});
xlim("padded");
ylim([0 1.2]);
title(t4,'$f_{c}$=$8$\,kHz, $f_{mod}$=$70$\,Hz','Interpreter','latex','FontSize',18);
set(gca,'FontSize',18,'FontName',  'Times New Roman');

xlabel(t,'$|\Phi|$','Interpreter','latex','FontSize',18);
ylabel(t,'$R$\,[asper]','Interpreter','latex','FontSize',18);
%%
N = length(YTest);
% y_test_pred = zeros(1, N);
% y_test_standardized = zeros(1, N);
% x_test = zeros(N, 9600);
% 
% for n=1:N
%     x_test(n, :) = XTest(:,:,:,n);
% end
% 
for n=1:N
%     y_test_pred(n) = predict(net, x_test(n, :));
%     y_test_standardized(n) = mean(acousticRoughness(XTest(:,:,:,n).', Fs));
end

% ml_model_rmse = rmse(y_test_pred.', YTest)
% standardized_model_rmse = sqrt(mean((YTest - y_test_standardized.').^2))

y = [0.0560 0.1346 0.6499]
% b = bar([1 2 3], [0.0560 0.5 0.6499], 'FaceColor', [0 0.4470 0.7410])
bar([1 2 3], y, 'FaceColor', [0.5 0.7 0.9])
% colormap([0.5 0.7 0.9; 0.3 0.5 0.8; 0.1 0.3 0.6]); % 各棒の色を指定
% % colorbar; % カラーバーを表示（オプション）
xticklabels({'CNN', 'RNN', 'standardized'});
xlabel('model architecture','Interpreter','latex','FontSize',16);
ylabel('RMSE','Interpreter','latex','FontSize',16);
for i = 1:3
    text(i, y(i), num2str(y(i), '%.4f'), ...
         'HorizontalAlignment', 'center', ...
         'VerticalAlignment', 'bottom');
end
set(gca,'FontSize',16,'FontName',  'Times New Roman');
exportgraphics(gca, '../../roughness_research/master_thesis/RMSE.pdf' );

%%
XT = zeros(1, 10, 1, 20); 
YT = zeros(1, 20); 
for n=1:20
    XT(:,:,:,n) = n;
    YT(n) = n;
end

testId = randperm(20-round(5),round(5))
XTes = XT(:,:,:,testId)
XT(:,:,:,testId) = []
YTes = YT(testId)
YT(testId) = []

%%
fileName = append(['audio/audio_FM_frequency_deviation/audio270.wav']);
%fileName = append('audio/audio_SAM_modulation_frequency/audio17.wav');
[x, Fs] = audioread(fileName);
%  YPred_DanielAndWeber_Fig10(n) = 
predict(net, x(1:9600).')
predict(net, x(4801:14400).')
predict(net, x(9601:19200).')
predict(net, x(14401:24000).')
predict(net, x(19201:28800).')
predict(net, x(24001:33600).')
predict(net, x(28801:38400).')
predict(net, x(33601:43200).')
predict(net, x(38401:48000).')

% XTest(1,:10,1,15)

% 21519
% 25506
% 39681
% 44190
% 51147
% 
% 2391
% 2834
% 4409
% 4910
% 5683
% 
% 2445/9=271..6

% 46635
%%

% YPred = predict(net,XTest);
% rmse = mean(abs(YTest-YPred)); % Root Mean Squared Error

function error = mae(predicted, actual)
    difference = predicted-actual;
    error = mean(abs(difference));
end

function error = rmse(predicted, actual)
    difference = predicted - actual;
    squaredDifference = difference .^ 2;
    meanSquaredDifference = mean(squaredDifference);
    error = sqrt(meanSquaredDifference);
end